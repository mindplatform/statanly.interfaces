"use strict"

const path = require('path');
const webpack = require('webpack');

module.exports = {
    entry: [
        'whatwg-fetch',
        './src/app.entry.js'
    ],
    output: {
        path: path.resolve(__dirname, 'public'),
        publicPath: '/',
        filename: './scripts/app.bundle.js'
    },
    resolve: {
        modules: [
            path.resolve(__dirname, './src/scripts'),
            "node_modules",
        ],
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    { loader: 'css-loader', options: { importLoaders: 1 } },
                    'postcss-loader',
                ]
            },
            {
                test: /\.less$/,
                exclude: /node_modules/,
                use: [
                    'style-loader',
                    { loader: 'css-loader', options: { importLoaders: 1 } },
                    'less-loader',
                    'postcss-loader',
                ],
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                include: path.resolve(__dirname, 'src'),
                query: {
                    presets: ['stage-0', 'es2015', 'react'],
                    plugins: ['transform-decorators-legacy']
                }
            },
        ]
    },
    node: {
        fs: "empty",
        process: "mock",
    },
    plugins: [
        new webpack.DefinePlugin({
            process: {
                env: {
                    NODE_ENV: JSON.stringify('development'),
                    APP_PORT: JSON.stringify(process.env.APP_PORT),
                    APP_LOG_LEVEL: JSON.stringify(process.env.APP_LOG_LEVEL)
                },
            },
        }),
    ]
}
