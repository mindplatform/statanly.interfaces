# API
## Methods
### Creating Table
path: `Table/Create`
method: `POST`
body: `[any]: raw table` (formats: _"application/vnd.ms-excel", "text/plain"_)
get params:
- `name`: String,
- `description`: String,
- `start`: Number,

### Updating Table
path: `Table/Update/{table_id}`
method: `PUT`
body: `table` (format: _serialized table model_)

### Deleting Table
path: `Table/Delete/{table_id}`
method: `DELETE`

### Adding Table Data
path: `Table/Load`
method: `POST`
body: `[any]: raw table` (formats: _"application/vnd.ms-excel", "text/plain"_)
get params:
- `id`: String,
- `start`: Number,

### Ping
path: `Echange/Ping`
method: `GET`
body: 
```
items:[
    {key:0,entityJson:null},
    {key:1,entityJson:null},
    {key:2,entityJson:null},
    {key:3,entityJson:null},
    {key:4,entityJson:null},
    {key:5,entityJson:null},
    {key:6,entityJson:null},
    {key:7,entityJson:null},
],
```

### Building Model 4 Table
path : `Model/Build`
method: `GET`
get params:
- `tableId`: String,
- `modelId`: String,

### Getting Model Status 4 Table
path: `Model/Ping`
method: `GET`
get params:
- `tableId`: String,
- `modelId`: String,

### Getting Model Results 4 Table
path: `Model/Calc`
method: `POST`
get params:
- `tableId`: String,
- `modelId`: String,
