module.exports = {
    rewrite: [
        { "from": "/Exchange/*", "to": "http://app.statanly.com/Exchange/$1" },
        { "from": "/Table/*", "to": "http://app.statanly.com/Table/$1" },
        { "from": "/Data/*", "to": "http://app.statanly.com/Data/$1" },
        { "from": "/Model/*", "to": "http://app.statanly.com/Model/$1" }
    ],
}
