import I from 'immutable'

import TABLE_COLUMN_TYPES from 'Types/Table/TableColumn.types'

const TableColumnRecord = I.Record({
    name:           String(),
    description:    String(),
    type:           TABLE_COLUMN_TYPES.get('String'),
})

export default class TableColumn extends TableColumnRecord {
    name:           string;
    description:    string;
    type:           number;

    constructor (props) {
        super(props)
    }
}
