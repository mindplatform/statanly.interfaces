import I from 'immutable'

import MathModelMap from './MathModelMap.model'

const MathModelSettingsRecord = I.Record({
    learningType:  Number(),
    problemType:   Number(),
    algorithmType: null,
    maps:          [],
    dependValue:   null,
})

export default class MathModelSettings extends MathModelSettingsRecord {
    learningType:   number;
    problemType:    number;
    algorithmType:  string;
    maps:           object;
    dependValue:    object;

    constructor (props = {}) {
        const maps = props.maps || []
        const dependValue = props.dependValue || []

        props.maps = I.List(maps.map(map => new MathModelMap(map)))
        props.dependValue = I.List(dependValue)

        super(props)
    }
}
