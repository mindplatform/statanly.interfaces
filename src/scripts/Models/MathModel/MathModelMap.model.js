import I from 'immutable'

const MathModelMapRecord = I.Record({
    height: Number(),
    width:  Number(),
    size:   Number(),
})

export default class MathModelMap extends MathModelMapRecord {
    height: number;
    width:  number;
    size:   number;

    constructor (props = {}) {
        super(props)
    }
}