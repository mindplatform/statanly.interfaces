import I from 'immutable'

const MathModelStatusRecord = I.Record({
    type: Number(),
    code: Number(),
})

export default class MathModelStatus extends MathModelStatusRecord {
    type: number;
    code: number;

    constructor (props = {}) {
        super(props)
    }
}
