import I from 'immutable'

import MathModelStatus from './MathModelStatus.model'
import MathModelSettings from './MathModelSettings.model'

const MathModelRecord = I.Record({
    id:               String(),
    name:             String(),
    createdDate:      String(),
    lastUpdate:       String(),
    status:           new MathModelStatus,
    isCalculated:     Boolean(),
    settings:         new MathModelSettings,
    serializedModel:  String(),
    isDeleted:        Boolean(),
})

export default class MathModel extends MathModelRecord {
    id:               string;
    name:             string;
    createdDate:      string;
    lastUpdate:       string;
    status:           object;
    isCalculated:     boolean;
    settings:         object;
    serializedModel:  string;
    isDeleted:        boolean;

    constructor (props = {}) {
        const settings = props.settings || {}
        const status = props.status || {}

        props.status = new MathModelStatus(status)
        props.settings = new MathModelSettings(settings)

        super(props)
    }
}
