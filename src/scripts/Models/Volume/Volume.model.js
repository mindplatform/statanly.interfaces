import I from 'immutable'

import MathModel from 'Models/MathModel/MathModel.model'
import TableColumn from 'Models/Table/TableColumn.model'
import VolumeStatus from 'Models/Volume/VolumeStatus.model'

const VolumeRecord = I.Record({
    userId:           String(),
    id:               String(),

    count:            Number(),
    createdDate:      String(),
    isDeleted:        Boolean(),
    description:      String(),
    lastUpdate:       null,
    models:           I.List(Array()),
    name:             String(),
    nameSpace:        String(),
    columns:          I.List(Array()),
    status:           null,
})

export default class Volume extends VolumeRecord {
    userId:           string;
    id:               string;

    count:            number;
    createdDate:      string;
    isDeleted:        boolean;
    description:      string;
    lastUpdate:       object;
    models:           object;
    name:             string;
    nameSpace:        string;
    columns:          object;

    status:           object;

    constructor (props = {}) {
        const models = props.models || []
        const columns = props.columns || []
        const status = props.status

        props.models = I.List(models.map(model => new MathModel(model)))
        props.columns = I.List(columns.map(column => new TableColumn(column)))
        props.status = new VolumeStatus(props.status)

        super(props)
    }
}
