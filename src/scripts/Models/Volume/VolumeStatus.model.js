import I from 'immutable'

const VolumeStatusRecord = I.Record({
    type: Number(),
    code: Number(),
})

export default class VolumeStatus extends VolumeStatusRecord {
    type: number;
    code: number;

    constructor (props = {}) {
        super(props)
    }
}
