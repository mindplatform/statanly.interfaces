import I from 'immutable'

const ENV = process.env.NODE_ENV
const GIT_HASH = process.env.GIT_HASH

const CONFIG_DEV = I.Map({
    API_ROOT_HTTP: null,
    GIT_HASH,
    CREADENTIALS: I.Map({
        cookie: I.Map({
            name: ".AspNet.ApplicationCookie",
            value: "ipxxV7b9g4nBKeFzNoueCBy0Amc22JylepRsNM4Rj3Pz8I2KQ-jJ-Wx798dT1VZeBHEYYeXc48nQcZMGPYXxyPRHugX3WczPH6b-Ij0TVtHlPGwxiIgAk_pSjTZtjlVA1EDMeQh9-JvpETeUPnC6_-n7eI-cnvju9TT7iGnv6or-w9ymal6iLf6o41Ck9JIvxVgl4R8OeMpK9Dp7jRcxSVb9Op1wgoN3U5uQmGPknUkbuYq5cNwpCtwnosf5-W3wwa1A1amNQrlGrGoM-DiFbPW95_CA5PLhm8psBkmGFp0PVkRVISSLvcBHxBPecXVtRQ4QsgWH9qIkKlEM3rj7dSlArhtdDyUu8c1AcH-pbl36ODwkf659scAIOpgA2Sw-DSmP3Rz8X-Sqd-5zZDzEvcRaBZGpT5F9RkmioVX08RKKDyiK1NbZ_nQuo3G2AcJLCcmUVTzyQd5_HtEeNrqfA4uPmnP57oYK1Z7DrvPuRY7kewXhZ31jSgUQms9Y-sO5",
            path: "/",
            domain: ".statanly.com",
        }),
    }),
})

const CONFIG_PROD = I.Map({
    API_ROOT_HTTP: null,
    GIT_HASH,
})

const CONFIG = ENV == 'development' ? CONFIG_DEV : CONFIG_PROD

export default CONFIG