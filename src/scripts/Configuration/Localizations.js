import i18n from 'i18next'
// import XHR from 'i18next-xhr-backend'
import LanguageDetector from 'i18next-browser-languagedetector'

i18n
  // .use(XHR)
  .use(LanguageDetector)
  .init({
    // we init with resources
    resources: {
      en: {
        translations: {}
      },
      ru: {
        translations: {}
      }
    },
    fallbackLng: 'ru',

    // have a common namespace used around the full app
    ns: ['translations'],
    defaultNS: 'translations',

    keySeparator: false, // we use content as keys

    interpolation: {
      escapeValue: false, // not needed for react!!
      formatSeparator: ','
    },

    // react: {
    //   wait: true
    // },

    // backend: {
    //   // load from i18next-gitbook repo
    //   loadPath: 'https://raw.githubusercontent.com/i18next/i18next-gitbook/master/locales/{{lng}}/{{ns}}.json',
    //   crossDomain: true
    // }
  })

export default i18n