import './AnalysisVolumeContainer.less'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import bem from 'Helpers/Bem/Bem.helper'
import cn from 'classnames'

import * as VolumesActions from 'Redux/Actions/Volumes/Volumes.actions'

import { withStyles } from 'material-ui/styles'

import ModelView from 'UI/Containers/Models/ViewModelInVolumeContainer.react'

const styles = theme => ({})

class AnalysisVolumeContainer extends Component {
    constructor (props) {
        super(props)
        this.blockName = 'AnalysisVolumeContainer'
        this.bem = bem.with(this.blockName)
    }

    renderModels () {
        const {
            addButton,
            addButtonIcon,
        } = this.props.classes

        const {
            volume,
        } = this.props

        return (
            <ul className={this.bem('Models-List')}>
                {volume.get('models').reverse().map((model,i) => (
                    <ModelView
                        key={`${i}-model`}
                        className={this.bem('Models-List-Item')}
                        model={model}
                        volume={volume}
                    />
                ))}
            </ul>
        )
    }

    render () {
        const isDeleted = this.props.volume.get('isDeleted')

        if (isDeleted)
            return null

        const {
            className,
        } = this.props

        return (
            <section className={cn(this.bem(), className)}>
                {this.renderModels()}
            </section>
        )
    }
}

AnalysisVolumeContainer.defaultProps = {
    className: "",
}

export default connect(
    state => ({}),
    dispatch => ({
        VolumesActions: bindActionCreators(VolumesActions, dispatch),
    }),
)(withStyles(styles)(AnalysisVolumeContainer))
