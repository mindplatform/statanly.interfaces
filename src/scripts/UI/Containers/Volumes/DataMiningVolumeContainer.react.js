import './DataMiningVolumeContainer.less'
import React, { Component } from 'react'

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import bem from 'Helpers/Bem/Bem.helper'

import VolumesDictionarySelector from 'Redux/Selectors/Volumes/Dictionary.selector'
import * as VolumesActions from 'Redux/Actions/Volumes/Volumes.actions'

import VolumeColumnsComponent from 'UI/Components/Volumes/VolumeColumnsComponent.react'
import ColumnsVisualisation from 'UI/Components/Volumes/Visualisation/Columns.d3'

import DataMiningBrush from 'UI/Components/Volumes/Visualisation/DataMiningBrush.d3'
import WindowResizer from 'Helpers/WindowResizer.helper'

class DataMiningVolumeContainer extends Component {
    constructor (props) {
        super(props)
        this.blockName = 'DataMiningVolumeContainer'
        this.bem = bem.with(this.blockName)

        this.state = {
            activeColumn: null,
            brushSelection: null,
        }

        this.handleColumnClick = ::this.handleColumnClick
        this.handleBrushEnd = ::this.handleBrushEnd
        this.renderVisualisations = ::this.renderVisualisations
    }

    handleColumnClick (nextActiveColumnName) {
        this.setState({
            activeColumn: nextActiveColumnName,
        })
        this.DataMiningBrush.reset()
    }

    handleBrushEnd (brushSelection) {
        this.setState({
            brushSelection,
        })
    }

    renderVisualisations () {
        const boundingRect = this.boxRef.getBoundingClientRect()

        const dictionary = this.props.volumeDictionary.dictionary
        const activeColumn = this.state.activeColumn

        this.ColumnsVisualisation.render({
            data: dictionary.get(activeColumn).toArray().map((value, index) => ({
                x: index,
                y: value[1],
            })),
            originWidth: boundingRect.width,
            originHeight: 500,
            rootNode: this.refSVGNode,
        })

        const brushSelection = this.state.brushSelection
        if (brushSelection) {
            this.ColumnsVisualisation.repaint({
                brushSelection,
            })
        }

        this.DataMiningBrush.render({
            originWidth: boundingRect.width,
            originHeight: 500,
        })
    }

    componentDidMount () {
        const {
            VolumesActions,
            volume,
        } = this.props

        this.windowResizer = new WindowResizer()
        this.windowResizer.addListener(this.renderVisualisations)
        this.ColumnsVisualisation = new ColumnsVisualisation
        this.DataMiningBrush = new DataMiningBrush(this.refSVGBrushNode, this.handleBrushEnd)

        VolumesActions.getVolumeDictionary({
            volume,
        })
    }

    componentWillReceiveProps (nextProps) {
        if (!this.state.activeColumn) {
            const firstColumnName = nextProps.volume.getIn(['columns', 0, 'name'])
            if (firstColumnName)
                this.setState({
                    activeColumn: firstColumnName,
                })
        }

    }

    componentDidUpdate (prevProps, prevState) {
        const {
            dictionary,
        } = this.props.volumeDictionary

        if (dictionary && this.state.activeColumn) {
            this.renderVisualisations()
        }

        const {
            brushSelection
        } = this.state
        if (brushSelection != prevState.brushSelection) {
            this.ColumnsVisualisation.repaint({
                brushSelection,
            })
        }
    }

    componentWillUnmount () {
        this.windowResizer.removeListener()
    }

    render () {
        const {
            volume,
        } = this.props

        return (
            <div ref={ref => this.boxRef = ref} className={this.bem()}>
                <VolumeColumnsComponent
                    volume={volume}
                    onSelectColumn={this.handleColumnClick}
                    selectedColumn={this.state.activeColumn}
                    className={this.bem('Columns')}
                />
                <div className={this.bem('SVGBox')}>
                    <svg
                        ref={ref => this.refSVGNode = ref}
                        className={this.bem('SVG')}
                    />
                    <svg
                        ref={ref => this.refSVGBrushNode = ref}
                        className={this.bem('SVGBrush')}
                    />
                </div>
            </div>
        )
    }
}

export default connect(
    state => ({
        volumeDictionary: VolumesDictionarySelector({ state }),
    }),
    dispatch => ({
        VolumesActions: bindActionCreators(VolumesActions, dispatch),
    }),
)(DataMiningVolumeContainer)
