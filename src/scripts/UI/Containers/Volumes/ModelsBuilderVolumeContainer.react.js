import './ModelsBuilderVolumeContainer.less'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { withRouter } from 'react-router'

import bem from 'Helpers/Bem/Bem.helper'
import cn from 'classnames'

import BlankModelSelector from 'Redux/Selectors/Models/BlankModel.selector'
import * as VolumesActions from 'Redux/Actions/Volumes/Volumes.actions'

import { withStyles } from 'material-ui/styles'
import ButtonBase from 'material-ui/ButtonBase'
import AddIcon from 'material-ui-icons/Add'

import CreateModelDialog from 'UI/Components/Models/CreateModelDialog.react'

import VOLUME_SWITCHER_TAB_TYPES from 'Consts/VOLUME_SWITCHER_TAB_TYPES.const'

const styles = theme => ({
    addButton: {},
    addButtonIcon: {
        fill: "#6a6a6a",
    },
})

class ModelsBuilderVolumeContainer extends Component {
    constructor (props) {
        super(props)
        this.blockName = 'ModelsBuilderVolumeContainer'
        this.bem = bem.with(this.blockName)

        this.state = {
            createModelDialogVisible: false,
        }

        this.handleAddModelClick = ::this.handleAddModelClick
        this.handleCreateModel = ::this.handleCreateModel
    }

    handleAddModelClick () {
        this.setState({
            createModelDialogVisible: true,
        })
    }

    handleCreateModel (model) {
        if (model) {
            const {
                volume,
            } = this.props

            this.props.VolumesActions.addVolumeModel({
                volume,
                model,
            })

            const {
                id,
            } = this.props.match.params

            this.props.history.push(`/v/${id}/${VOLUME_SWITCHER_TAB_TYPES.get('analysis')}`)
        }

        this.setState({
            createModelDialogVisible: false,
        })
    }

    renderHeader () {
        return (
            <CreateModelDialog
                visible={this.state.createModelDialogVisible}
                volume={this.props.volume}
                model={this.props.blankModel}
                onRequestClose={this.handleCreateModel}
                onRequestCreate={this.handleCreateModel}
            />
        )
    }

    renderModels () {
        const {
            addButton,
            addButtonIcon,
        } = this.props.classes

        const {
            volume,
        } = this.props

        return (
            <ul className={this.bem('Models-List')}>
                <li className={cn(this.bem('Models-List-Item'), 'Create')} onClick={this.handleAddModelClick}>
                    <ButtonBase focusRipple className={addButton}>
                        <AddIcon className={addButtonIcon}/>
                    </ButtonBase>
                </li>
            </ul>
        )
    }

    render () {
        const isDeleted = this.props.volume.get('isDeleted')

        if (isDeleted)
            return null

        const {
            className,
        } = this.props

        return (
            <section className={cn(this.bem(), className)}>
                {this.renderHeader()}
                {this.renderModels()}
            </section>
        )
    }
}

ModelsBuilderVolumeContainer.defaultProps = {
    className: "",
    onCreateModel: () => {},
}

export default withRouter(connect(
    state => ({
        blankModel: BlankModelSelector({ state }),
    }),
    dispatch => ({
        VolumesActions: bindActionCreators(VolumesActions, dispatch),
    }),
)(withStyles(styles)(ModelsBuilderVolumeContainer)))
