import './ViewModelInVolumeContainer.less'
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import _b from 'bem-cn'

import { withRouter } from 'react-router'
import { Link } from 'react-router-dom'

import * as ModelsActions from 'Redux/Actions/Models/Models.actions'
import * as VolumesActions from 'Redux/Actions/Volumes/Volumes.actions'

import { withStyles } from 'material-ui/styles'
import Button from 'material-ui/Button'
import IconButton from 'material-ui/IconButton'
import DeleteIcon from 'material-ui-icons/Delete'

import MATH_MODEL_PROBLEM_TYPES from 'Types/MathModel/MathModel.ProblemType.types'
import TABLE_COLUMN_TYPES from 'Types/Table/TableColumn.types'
import MATH_MODEL_STATUS_TYPES from 'Types/MathModel/MathModel.StatusType.types'

import classNames from 'classnames'

import green from 'material-ui/colors/green'
import red from 'material-ui/colors/red'
import Bookmark from 'material-ui-icons/Bookmark'

import SpinnerSimple from 'UI/Components/Spinner/Spinner.simple.react'

const styles = theme => ({
    buttonDelete: {
        color: "#888",
    },
    buttonRestore: {
        color: "#888",
    },
    buttonBuild: {},
    buttonShow: {
        color: "#59af50",
    },
    iconCalculated: {},
    iconCalculatedActive: {
        fill: green[500],
    },
    iconCalculatedInactive: {
        fill: "#bbb",
    },
    iconFailActive: {
        fill: red[500],
    },
    iconFailInactive: {
        fill: "#bbb",
    },
})

class ViewModelInVolumeContainer extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'ViewModelInVolumeContainer'
        this._b = _b(this.boxClassName)

        this.state = {}

        this.handleModelDelete = ::this.handleModelDelete
        this.handleModelRestore = ::this.handleModelRestore

        this.handleModelBuild = ::this.handleModelBuild
        this.handleModelShow = ::this.handleModelShow

        this.handleModelStatus = ::this.handleModelStatus

        this.handleModelStatus(props)
    }

    handleModelDelete (model) {
        const {
            volume,
        } = this.props
        model = model.setIn(['isDeleted'], true)

        this.props.VolumesActions.updateVolumeModel({
            volume,
            model,
        })
    }

    handleModelRestore (model) {
        const {
            volume,
        } = this.props
        model = model.setIn(['isDeleted'], false)

        this.props.VolumesActions.updateVolumeModel({
            volume,
            model,
        })
    }

    handleModelBuild (model) {
        const {
            volume,
        } = this.props

        this.props.ModelsActions.buildModel({
            volume,
            model,
        })
    }

    handleModelShow () {}

    handleModelStatus (props) {
        const {
            volume,
            model,
            ModelsActions,
        } = props

        const GET_MODEL_TIMEOUT = 5e3

        if (model) {
            const modelStatusType = model.getIn(['status', 'type'])
            if (modelStatusType == MATH_MODEL_STATUS_TYPES.get('InProgress')) {
                setTimeout(() => ModelsActions.getModel({
                    volume,
                    model,
                }), GET_MODEL_TIMEOUT)
            }
        }
    }

    componentWillReceiveProps (nextProps) {
        this.handleModelStatus(nextProps)
    }

    renderModelLabel (model) {
        const {
            iconCalculated,
            iconCalculatedActive,
            iconCalculatedInactive,
            iconFailActive,
            iconFailInactive,
        } = this.props.classes

        if (model.getIn(['status', 'type']) == MATH_MODEL_STATUS_TYPES.get('Сalculated'))
            return (
                <Bookmark
                    className={classNames(
                        iconCalculated,
                        model.getIn(['isDeleted']) ? iconCalculatedInactive : iconCalculatedActive,
                        this._b('CalculatedLabel').toString(),
                    )}
                />
            )
        else if (model.getIn(['status', 'type']) == MATH_MODEL_STATUS_TYPES.get('Fail'))
            return (
                <Bookmark
                    className={classNames(
                        iconCalculated,
                        model.getIn(['isDeleted']) ? iconFailInactive : iconFailActive,
                        this._b('FailLabel').toString(),
                    )}
                />
            )

        return null
    }

    renderModelType (model) {
        return (
            <span className={this._b('Type')}>
                { MATH_MODEL_PROBLEM_TYPES.findEntry((v, i) => v == model.getIn(['settings', 'problemType']), null, [])[0] }
            </span>
        )
    }

    renderModelName (model) {
        return (
            <span className={this._b('Name')}>
                { model.get('name') }
            </span>
        )
    }

    renderDependValue (model) {
        const columnIndex = model.getIn(['settings', 'dependValue', 0])
        const column = this.props.volume.getIn(['columns', columnIndex])

        if (!column)
            return null

        return (
            <div className={this._b('DependValue')}>
                <div className={this._b('DependValue')('Column')}>
                    <span className={this._b('DependValue')('Column')('Index')}>{columnIndex + 1}</span>
                    <span className={this._b('DependValue')('Column')('Name')}>{column.get('name')}</span>
                    <span className={this._b('DependValue')('Column')('Type')}>{TABLE_COLUMN_TYPES.entrySeq().find(e => e[1] == column.get('type'))[0]}</span>
                </div>
            </div>
        )
    }

    renderModelStatus (model) {
        const statuses = []

        if (model.getIn(['status', 'type']) == MATH_MODEL_STATUS_TYPES.get('InProgress')) {
            statuses.push((
                <div key="InProgressLabel" className={this._b('Status')('InProgressLabel')}>
                    In Progress
                </div>
            ))
            statuses.push((
                <SpinnerSimple key="InProgressIcon" className={this._b('Status')('Spinner').toString()}/>
            ))
        }

        return (
            <div className={this._b('Status')}>
                {statuses}
            </div>
        )
    }

    renderModelControls (model) {
        const {
            buttonDelete,
            buttonRestore,
            buttonBuild,
            buttonShow,
        } = this.props.classes

        const buttons = []

        if (model.getIn(['isDeleted'])) {
            buttons.push(
                <Button
                    key="RestoreButton"
                    className={classNames(buttonRestore, this._b('Controls')('Button'))}
                    onClick={() => this.handleModelRestore(model)}
                    aria-label="Restore"
                >
                    Restore
                </Button>
            )

        } else {

            if (model.getIn(['status', 'type']) == MATH_MODEL_STATUS_TYPES.get('InProgress')) {
                return this.renderModelStatus(model)
            }

            buttons.push(
                <Button
                    key="DeleteButton"
                    className={classNames(buttonDelete, this._b('Controls')('Button'))}
                    onClick={() => this.handleModelDelete(model)}
                    aria-label="Delete"
                >
                    Delete
                </Button>
            )

            buttons.push(
                <Button
                    key="BuildButton"
                    className={classNames(buttonBuild, this._b('Controls')('Button'))}
                    onClick={() => this.handleModelBuild(model)}
                    aria-label="Build"
                >
                    Build
                </Button>
            )

            // if (model.getIn(['settings', 'problemType']) == 3) {
                if (model.getIn(['status', 'type']) == MATH_MODEL_STATUS_TYPES.get('Сalculated')) {
                    const {
                        id,
                    } = this.props.match.params
                    buttons.push(
                        <Link
                            to={`/m/${id}/${model.get('id')}`}
                            key="ShowButton"
                            className={this._b('Controls')('Link')}
                            onClick={() => this.handleModelShow(model)}
                            aria-label="Show"
                        >
                            Show
                        </Link>
                    )
                }
            // }
        }

        return (
            <div className={this._b('Controls')}>
                {buttons}
            </div>
        )
    }

    render () {
        const {
            className,
            model,
        } = this.props

        return (
            <div className={this._b.mix(className)}>
                {this.renderModelLabel(model)}
                {this.renderModelType(model)}
                {this.renderModelName(model)}
                {/*this.renderDependValue(model)*/}
                {this.renderModelControls(model)}
            </div>
        )
    }
}

ViewModelInVolumeContainer.propTypes ={
    model: PropTypes.any.isRequired,
}

ViewModelInVolumeContainer.defaultProps = {
    className: "",
}

export default withRouter(connect(
    state => ({}),
    dispatch => ({
        VolumesActions: bindActionCreators(VolumesActions, dispatch),
        ModelsActions: bindActionCreators(ModelsActions, dispatch),
    }),
)(withStyles(styles)(ViewModelInVolumeContainer)))
