import './ViewModelTableAnalysisContainer.less'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import _b from 'bem-cn'

import { withStyles } from 'material-ui/styles'
import Radio, { RadioGroup } from 'material-ui/Radio'
import IconButton from 'material-ui/IconButton'
import CropIcon from 'material-ui-icons/Crop'
import CloseIcon from 'material-ui-icons/Close'

import Dialog from 'material-ui/Dialog'
import Slide from 'material-ui/transitions/Slide'
import AppBar from 'material-ui/AppBar'
import Toolbar from 'material-ui/Toolbar'
import Typography from 'material-ui/Typography'

import FileReaderHelper from 'Helpers/FileReader/FileReader.helper'

import CalculatedModelTableSelector from 'Redux/Selectors/Models/CalculatedModelTable.selector'
import * as ModelsActions from 'Redux/Actions/Models/Models.actions'

import AppWaitDialog from 'UI/Components/AppWaitDialog/AppWaitDialog.react'
import HeatMaps from 'UI/VisualComponents/HeatMaps/HeatMaps.react'

import {
    AutoSizer,
    Grid,
} from 'react-virtualized'

function Transition (props) {
    return <Slide direction="up" {...props} />
}

const styles = theme => ({
    group: {},
    uploadInput: {
        display: 'none',
    },
    cropIcon: {
        fill: '#3f51b5',
    },
    appBar: {
        position: 'relative',
        backgroundColor: "rgba(19, 32, 40, 0.85)",
    },
    title: {
        flex: 1,
        fontWeight: 200,
        textAlign: "center",
    },
    closeButton: {},
})

class ViewModelTableAnalysisContainer extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'ViewModelTableAnalysisContainer'
        this._b = _b(this.boxClassName)

        this.state = {
            mapResolutionValue: "0",
            featuresDialogVisible: false,
        }

        this.handleUpload = ::this.handleUpload
    }

    handleUpload () {
        const {
            volume,
            model,
        } = this.props

        if (this.refInput.files && this.refInput.files[0])
            new FileReaderHelper(this.refInput.files[0], file => {
                this.props.ModelsActions.calculateModel({
                    volume,
                    model,
                    file,
                })
            })
    }

    componentDidMount () {
        this.refInput.click()
    }

    componentWillUnmount () {
        this.props.ModelsActions.setCalculatedModel({
            sourceModel: null,
            calculatedModel: null,
        })
    }

    renderDeletedModel () {
        return (
            <div className={this._b("Deleted")}>
                Deleted
            </div>
        )
    }

    renderNotFound () {
        return (
            <div className={this._b("NotFound")}>
                Not Found
            </div>
        )
    }

    _noContentRenderer() {
        return (
            <div className={this._b('No-Cells')}>
                no data
            </div>
        )
    }

    _cellRenderer({columnIndex, key, rowIndex, style}) {
        const { calculatedModel } = this.props
        const { model } = this.props
        const dependValueIndex = model.getIn(['settings', 'dependValue', 0])
        return (
            <div className={this._b('Cell').state({ active: dependValueIndex == columnIndex })} key={key} style={style}>
                {calculatedModel.getIn([0, rowIndex, columnIndex])}
            </div>
        )
    }

    renderTable () {
        const { calculatedModel } = this.props
        if (!calculatedModel)
            return null

        let scrollToColumn
        let scrollToRow
        const columnCount = calculatedModel.getIn([0,0]).size

        return (
            <AutoSizer>
                {({ width, height }) => (
                    <Grid
                        cellRenderer={::this._cellRenderer}
                        className={this._b('Grid').toString()}
                        columnWidth={width/columnCount}
                        columnCount={columnCount}
                        height={height}
                        noContentRenderer={::this._noContentRenderer}
                        overscanColumnCount={10}
                        overscanRowCount={10}
                        rowHeight={40}
                        rowCount={calculatedModel.getIn([0]).size}
                        scrollToColumn={scrollToColumn}
                        scrollToRow={scrollToRow}
                        width={width}
                    />
                )}
            </AutoSizer>
        )
    }

    render () {
        const {
            className,
            volume,
            model,
        } = this.props

        const isVolumeDeleted = volume.get('isDeleted')
        const isModelDeleted = model.get('isDeleted')

        if (isVolumeDeleted || isModelDeleted)
            return this.renderDeletedModel()

        const {
            sourceModel,
            calculatedModel,
        } = this.props

        if (sourceModel && model.get('id') != sourceModel.get('id'))
            return this.renderNotFound()

        const isInProgress = !sourceModel && !calculatedModel

        const {
            appBar,
            uploadInput,
        } = this.props.classes

        return (
            <section className={this._b.mix(className)}>
                {this.renderTable()}
                <form className={this._b('Form')}>
                    <input ref={ref => this.refInput = ref} hidden name="table" accept="" className={uploadInput} id="file" type="file" onChange={this.handleUpload}/>
                </form>
                <AppWaitDialog
                    in={isInProgress}
                    absolute
                />
            </section>
        )
    }
}

ViewModelTableAnalysisContainer.propTypes = {}

export default connect(
    state => CalculatedModelTableSelector({ state }),
    dispatch => ({
        ModelsActions: bindActionCreators(ModelsActions, dispatch),
    }),
)(withStyles(styles)(ViewModelTableAnalysisContainer))
