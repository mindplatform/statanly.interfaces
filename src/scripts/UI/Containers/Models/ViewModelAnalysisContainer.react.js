import './ViewModelAnalysisContainer.less'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import _b from 'bem-cn'

import { withStyles } from 'material-ui/styles'
import Radio, { RadioGroup } from 'material-ui/Radio'
import IconButton from 'material-ui/IconButton'
import CropIcon from 'material-ui-icons/Crop'
import CloseIcon from 'material-ui-icons/Close'

import Dialog from 'material-ui/Dialog'
import Slide from 'material-ui/transitions/Slide'
import AppBar from 'material-ui/AppBar'
import Toolbar from 'material-ui/Toolbar'
import Typography from 'material-ui/Typography'

import CalculatedModelSelector from 'Redux/Selectors/Models/CalculatedModel.selector'
import * as ModelsActions from 'Redux/Actions/Models/Models.actions'

import AppWaitDialog from 'UI/Components/AppWaitDialog/AppWaitDialog.react'
import HeatMaps from 'UI/VisualComponents/HeatMaps/HeatMaps.react'

function Transition (props) {
    return <Slide direction="up" {...props} />
}

const styles = theme => ({
    group: {},
    cropIcon: {
        fill: '#3f51b5',
    },
    appBar: {
        position: 'relative',
        backgroundColor: "rgba(19, 32, 40, 0.85)",
    },
    title: {
        flex: 1,
        fontWeight: 200,
        textAlign: "center",
    },
    closeButton: {},
})

class ViewModelAnalysisContainer extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'ViewModelAnalysisContainer'
        this._b = _b(this.boxClassName)

        this.state = {
            mapResolutionValue: "0",
            featuresDialogVisible: false,
        }

        this.handleMapResolutionValueChange = ::this.handleMapResolutionValueChange
        this.handleFeaturesDialogShow = ::this.handleFeaturesDialogShow
        this.handleFeaturesDialogClose = ::this.handleFeaturesDialogClose
    }

    handleMapResolutionValueChange (nextValue) {
        this.setState({
            mapResolutionValue: nextValue,
        })
    }

    handleFeaturesDialogShow () {
        this.setState({
            featuresDialogVisible: true,
        })
    }

    handleFeaturesDialogClose () {
        this.setState({
            featuresDialogVisible: false,
        })
    }

    componentDidMount () {
        const {
            volume,
            model,
        } = this.props

        this.props.ModelsActions.calculateModel({
            volume,
            model,
        })
    }

    componentWillUnmount () {
        this.props.ModelsActions.setCalculatedModel({
            sourceModel: null,
            calculatedModel: null,
        })
    }

    renderDeletedModel () {
        return (
            <div className={this._b("Deleted")}>
                Deleted
            </div>
        )
    }

    renderNotFound () {
        return (
            <div className={this._b("NotFound")}>
                Not Found
            </div>
        )
    }

    renderMapControls () {
        const {
            group,
            cropIcon,
        } = this.props.classes

        const mapResolutionValue = this.state.mapResolutionValue

        return (
            <div
                className={this._b('ControlsBox')}
            >
                <div className={this._b('ControlsBox')('Controls')}>
                    <div
                        className={this._b('ControlsBox')('Controls')('Item')}
                        onClick={() => this.handleMapResolutionValueChange(0)}
                    >
                        <Radio
                            value="0"
                            label="Low"
                            checked={mapResolutionValue == 0}
                        />
                        <span>Low</span>
                    </div>
                    <div
                        className={this._b('ControlsBox')('Controls')('Item')}
                        onClick={() => this.handleMapResolutionValueChange(1)}
                    >
                        <Radio
                            value="1"
                            label="Middle"
                            checked={mapResolutionValue == 1}
                        />
                        <span>Middle</span>
                    </div>
                    <div
                        className={this._b('ControlsBox')('Controls')('Item')}
                        onClick={() => this.handleMapResolutionValueChange(2)}
                    >
                        <Radio
                            value="2"
                            label="High"
                            checked={mapResolutionValue == 2}
                        />
                        <span>High</span>
                    </div>
                </div>
                <div className={this._b('ControlsBox')('CropTool')}>
                    <CropIcon className={cropIcon}/>
                    <div
                        className={this._b('ControlsBox')('CropTool')('ShowButton')}
                        onClick={this.handleFeaturesDialogShow}
                    >
                        Show Features
                    </div>
                </div>
            </div>
        )
    }

    renderMap () {
        const calculatedModelMinValue = this.props.calculatedModelMinValue
        const calculatedModelMaxValue = this.props.calculatedModelMaxValue
        const calculatedModel = this.props.calculatedModel
        if (!calculatedModel || !calculatedModel.size)
            return null

        const {
            model,
        } = this.props

        const mapResolutionValue = +this.state.mapResolutionValue

        return (
            <HeatMaps
                className={this._b('HeatMaps').toString()}
                min={calculatedModelMinValue[mapResolutionValue]}
                max={calculatedModelMaxValue[mapResolutionValue]}
                data={calculatedModel.get(mapResolutionValue).toJS()}
                dimensions={model.getIn(['settings', 'maps', mapResolutionValue])}
            />
        )
    }

    renderToolBar () {
        const {
            title,
            closeButton,
        } = this.props.classes

        return (
            <Toolbar>
                <IconButton onClick={this.handleFeaturesDialogClose} className={closeButton} aria-label="Close" color="contrast">
                    <CloseIcon />
                </IconButton>
                <Typography type="headline" color="inherit" className={title}>
                    Selected data set features
                </Typography>
            </Toolbar>
        )
    }

    render () {
        const {
            className,
            volume,
            model,
        } = this.props

        const isVolumeDeleted = volume.get('isDeleted')
        const isModelDeleted = model.get('isDeleted')

        if (isVolumeDeleted || isModelDeleted)
            return this.renderDeletedModel()

        const {
            sourceModel,
            calculatedModel,
        } = this.props

        if (sourceModel && model.get('id') != sourceModel.get('id'))
            return this.renderNotFound()

        const isInProgress = !sourceModel && !calculatedModel

        const {
            appBar,
        } = this.props.classes

        return (
            <section className={this._b.mix(className)}>
                {this.renderMapControls()}
                {this.renderMap()}
                <AppWaitDialog
                    in={isInProgress}
                    absolute
                />
                <Dialog
                    fullScreen
                    open={this.state.featuresDialogVisible}
                    transition={Transition}
                    onRequestClose={this.handleFeaturesDialogClose}
                    classes={{
                        paper: this._b('FeaturesDialogPaper').toString(),
                    }}
                >
                    <AppBar className={appBar}>
                        {this.renderToolBar()}
                    </AppBar>

                </Dialog>
            </section>
        )
    }
}

ViewModelAnalysisContainer.propTypes = {}

export default connect(
    state => CalculatedModelSelector({ state }),
    dispatch => ({
        ModelsActions: bindActionCreators(ModelsActions, dispatch),
    }),
)(withStyles(styles)(ViewModelAnalysisContainer))
