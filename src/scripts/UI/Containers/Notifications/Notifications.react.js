import './Notifications.less'
import React, { Component } from 'react'

import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import _b from 'bem-cn'

import NotificationsSelector from 'Redux/Selectors/Notifications/Notifications.selector'

import MainThead from 'UI/Extra/Notifications/MainThread.react'

class Notifications extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'Notifications'
        this._b = _b(this.boxClassName)
    }

    render () {
        return (
            <section className={this._b.mix(this.props.className)}>
                <MainThead
                    visible
                    notification={this.props.lastNotifications.last()}
                />
            </section>
        )
    }
}

export default withRouter(connect(
    state => NotificationsSelector({ state }),
    dispatch => ({}),
)(Notifications))
