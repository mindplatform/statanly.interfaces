import './ShareMapData.less'
import React, { Component, PropTypes, } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import _b from 'bem-cn'

class ShareMapData extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'ShareMapData'
        this._b = _b(this.boxClassName)

        this.buttonNames = [
            'COPY',
            'OK'
        ]

        this.state = {
            buttonNameIndex: 0,
        }
    }

    componentDidMount () {
        this.refInput.select()
    }

    handleCopy () {
        if (this.state.buttonNameIndex)
            return this.props.closeModal()

        try {
            this.refInput.select()
            const successful = document.execCommand('copy')
            if (successful) {
                return this.props.closeModal()
            }
        } catch (e) {}

        this.setState({
            buttonNameIndex: 1,
        })
    }

    render () {
        const {
            linkShare,
        } = this.props

        return (
            <div className={this._b}>
                <p className={this._b('Title')}>
                    Share market state
                </p>
                <p className={this._b('SubTitle')}>
                    {
                        this.state.buttonNameIndex
                            ? 'copy the link below'
                            : null
                    }
                </p>
                <input 
                    ref={ref => this.refInput = ref}
                    className={this._b('ShareLinkInput')}
                    value={linkShare}
                />
                <div className={this._b('OkButton')} onClick={() => this.handleCopy()}>
                    {this.buttonNames[this.state.buttonNameIndex]}
                </div>
            </div>
        )
    }
}

export default connect(
    state => ({ }),
    dispatch => ({})
)(ShareMapData)