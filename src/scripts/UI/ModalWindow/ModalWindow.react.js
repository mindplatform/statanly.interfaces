import './ModalWindow.less'
import React, { Component, PropTypes, } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import _b from 'bem-cn'

import ModalWindowSelector from 'Redux/Selectors/ModalWindow/ModalWindow.selector'
import * as ModalWindowActions from 'Redux/Actions/ModalWindow/ModalWindow.actions'

import ShareMapData from './Bodies/ShareMapData.react'
import {
    MODAL_BOIES_TYPES,
} from 'Consts/ModalWindowTypes.const'

class ModalWindow extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'ModalWindow'
        this._b = _b(this.boxClassName)
    }

    handleCloseModal (bodyType) {
        this.props.ModalWindowActions.hideModalWindow({
            bodyType,
        })
    }

    renderByType () {
        const {
            bodyType,
            props,
        } = this.props

        switch (bodyType) {
            case MODAL_BOIES_TYPES.get('SHARE_MAP_DATA'):
                return (
                    <ShareMapData 
                        {...props} 
                        closeModal={() => this.handleCloseModal(bodyType)}
                    />
                )
        }

        return null
    }

    render () {
        const {
            active,
        } = this.props

        if (!active)
            return null

        return (
            <div className={this._b}>
                {this.renderByType()}
            </div>
        )
    }
}

export default connect(
    state => ModalWindowSelector({ state }),
    dispatch => ({
        ModalWindowActions: bindActionCreators(ModalWindowActions, dispatch),
    })
)(ModalWindow)