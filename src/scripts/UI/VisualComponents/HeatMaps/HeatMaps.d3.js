import * as d3 from 'd3'
import * as d3ScaleChromatic from 'd3-scale-chromatic'

export default class HeatMaps {
  constructor ({
    svgRoot,
  }) {

    this.c = {}
    this.c.cellSize = 25

    this.v = {}
    this.v.svg = d3.select(svgRoot)

    this.s = {}
    this.s.cellColors = d3ScaleChromatic.interpolateSpectral
  }

  updateDimensions ({
    width,
    height,
  }) {

    this.v.width = width
    this.v.height = height

    this.v.svg
      .attr('width', d3.max(data, d => d.x) * this.c.cellSize)
      .attr('height', d3.max(data, d => d.y) * this.c.cellSize)
  }

  fillWithData (data) {

    const that = this

    if (!data)
      return

    data = data.toJS ? data.toJS() : data

    this.v.svg
      .attr('width', d3.max(data, d => d.x) * this.c.cellSize)
      .attr('height', d3.max(data, d => d.y) * this.c.cellSize)

    this.colorScale = d3.scaleLinear().domain([d3.min(data, d => d.i), d3.max(data, d => d.i)]).range([1,0])

    this.v.cells = this.v.svg.selectAll(".cell")
    .data(data, d => d.x+':'+d.y)

    this.v.cells.enter().append("rect")
      .attr("x", d => d.x * this.c.cellSize)
      .attr("y", d => d.y * this.c.cellSize)
      .attr("rx", 4)
      .attr("ry", 4)
      .attr("class", "cell bordered")
      .attr("width", this.c.cellSize)
      .attr("height", this.c.cellSize)
      .style("fill", d => this.s.cellColors(this.colorScale(d.i)))
  }
}