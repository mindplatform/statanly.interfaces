import './HeatMaps.less'
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import bem from 'Helpers/Bem/Bem.helper'
import cn from 'classnames'

import h337 from 'heatmap.js'

import WindowResizer from 'Helpers/WindowResizer.helper'
import Brush from './HeatMaps.Brush.d3'

class HeatMaps extends Component {
    constructor (props) {
        super(props)
        this.blockName = 'HeatMaps'
        this.bem = bem.with(this.blockName)

        this.fillWithData = ::this.fillWithData
        this.onSelectRegion = ::this.onSelectRegion
    }

    getStepDimensions (dimensions) {
        const rect = this.refBaseRoot.getBoundingClientRect()

        const baseWidth = rect.width
        const baseHeight = rect.height

        const mapWidth = dimensions.get('width')
        const mapHeight = dimensions.get('height')

        const stepWidth = baseWidth / mapWidth
        const stepHeight = baseHeight / mapHeight

        return {
            baseWidth,
            baseHeight,
            stepWidth,
            stepHeight,
        }
    }

    onSelectRegion (selection) {
        const {
            stepWidth,
            stepHeight,
        } = this.getStepDimensions(this.props.dimensions)

        if (selection) {
            selection = selection.map(point => point.map((v, i) => Math.ceil(v / (i == 0 ? stepWidth : stepHeight))))
            console.log('selection', selection)
        }
    }

    fillWithData (props = this.props) {
        const {
            min,
            max,
            data,
            dimensions,
        } = props

        const {
            baseWidth,
            baseHeight,
            stepWidth,
            stepHeight,
        } = this.getStepDimensions(dimensions)

        const canvas = document.getElementsByTagName("canvas")[0]
        if (canvas && ~canvas.className.indexOf('heatmap-canvas')) {
            canvas.remove()
        }

        this.heatmapInstance = h337.create({
            container: this.refBaseRoot,
        })

        this.heatmapInstance.setData({
            min,
            max,
            data: data.map(point => Object.assign(
                {},
                point,
                {
                    x: point.x * stepWidth,
                    y: point.y * stepHeight,
                },
            )),
        })
    }

    componentDidMount () {
        this.windowResizer = new WindowResizer()
        this.windowResizer.addListener(this.fillWithData)
        this.fillWithData()

        this.brush = new Brush(this.refSVGRoot, this.onSelectRegion)
    }

    componentWillReceiveProps (nextProps) {
        if (nextProps.data != this.props.data) {
            this.fillWithData(nextProps)
        }
    }

    componentWillUnmount () {
        this.windowResizer.removeListener()
    }

    render () {
        const {
            className,
        } = this.props

        return (
            <section
                ref={ref => this.refBaseRoot = ref}
                className={cn(this.bem(), className)}>
                <svg ref={ref => this.refSVGRoot = ref} className={this.bem('SVG')}/>
            </section>
        )
    }
}

HeatMaps.propTypes = {
    min: PropTypes.number.isRequired,
    max: PropTypes.number.isRequired,
    data: PropTypes.array.isRequired,
    dimensions: PropTypes.any.isRequired,
}

export default HeatMaps
