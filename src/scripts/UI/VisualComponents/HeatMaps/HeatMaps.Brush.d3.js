import * as d3 from 'd3'

export default class HeatMapBrush {
    constructor (svgRoot, onBrushEnd) {
        this.svg = d3.select(svgRoot)

        this.brushed = ::this.brushed
        this.brushended = ::this.brushended
        this.onBrushEnd = onBrushEnd
        this.createBrush()
    }

    createBrush () {
        this.brush = d3.brush()
            .on("start brush", this.brushed)
            .on("end", this.brushended)

        this.svg.append("g")
            .attr("class", "brush")
            .call(this.brush)
    }

    brushed () {
        // in process
    }

    brushended () {
        this.onBrushEnd(d3.event.selection)
    }
}
