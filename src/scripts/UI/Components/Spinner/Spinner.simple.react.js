import './Spinner.simple.less'
import React, { Component } from 'react'
import PropTypes from 'prop-types'

import bem from 'Helpers/Bem/Bem.helper'

import { withStyles } from 'material-ui/styles'

import Transition from 'react-transition-group/Transition'
import classNames from 'classnames'

import blue from 'material-ui/colors/blue'
import AutorenewIcon from 'material-ui-icons/Autorenew'

const AnimationDuration = .6e3

const styles = theme => ({
    iconInProgress: {
        fill: blue[500],
    },
})

class SpinnerSimple extends Component {
    constructor (props) {
        super(props)
        this.blockName = "SpinnerSimple"
        this.bem = bem.with(this.blockName)
    }

    render () {
        const {
            className,
        } = this.props
        const {
            iconInProgress,
        } = this.props.classes

        return (
            <Transition key="InProgressIcon" timeout={AnimationDuration}>
                <AutorenewIcon className={classNames(className, iconInProgress, this.bem())}/>
            </Transition>
        )
    }
}

SpinnerSimple.defaultProps = {
    className: "",
}

export default withStyles(styles)(SpinnerSimple)
