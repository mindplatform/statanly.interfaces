import './TooltipsMaster.less'
import React, { 
    Component,
    Children,
    cloneElement,
} from 'react'
import PropTypes from 'prop-types'
import _b from 'bem-cn'
import { CSSTransitionGroup } from 'react-transition-group'

import TOOLTIP from 'Consts/Tooltip.const'

class TooltipsMaster extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'TooltipsMaster'
        this._b = _b(this.boxClassName)

        this.handleShowTooltip = ::this.handleShowTooltip
        this.handleHideTooltip = ::this.handleHideTooltip

        this.state = {
            currentTooltip: {},
        }
    }

    componentDidMount () {
        this.props.tooltipEventEmitter.addListener(TOOLTIP.getIn(['EVENTS', 'SHOW']), this.handleShowTooltip)
        this.props.tooltipEventEmitter.addListener(TOOLTIP.getIn(['EVENTS', 'HIDE']), this.handleHideTooltip)
    }

    componentWillUnmount () {
        this.props.tooltipEventEmitter.removeListener(TOOLTIP.getIn(['EVENTS', 'SHOW']), this.handleShowTooltip)
        this.props.tooltipEventEmitter.removeListener(TOOLTIP.getIn(['EVENTS', 'HIDE']), this.handleHideTooltip)
    }

    handleShowTooltip (type, data, rect) {
        console.log('TOOLTIP', 'SHOW', type, data, rect)
        this.setState({
            currentTooltip: {
                type,
                data,
                rect,
            }
        })
    }

    handleHideTooltip (type, data) {
        console.log('TOOLTIP', 'HIDE', type, data)
        this.setState({
            currentTooltip: {},
        })
    }

    render () {
        const {
            children,
        } = this.props

        const {
            type,
            data,
            rect,
        } = this.state.currentTooltip

        return (
            <section className={this._b}>
                <div className={this._b('TooltipBox')}>
                    <CSSTransitionGroup 
                        transitionLeaveTimeout={0} 
                        transitionEnterTimeout={0} 
                        transitionName="TooltipTransitions"
                    >
                        {
                            Children.map(children, Child => {
                                if (Child.props.type == type) {
                                    return cloneElement(Child, Object.assign(
                                        {},
                                        Child.props,
                                        {
                                            data,
                                            rect,
                                        }
                                    ))
                                }
                            })
                        }
                    </CSSTransitionGroup>
                </div>
            </section>
        )
    }
}

TooltipsMaster.propTypes = {
    tooltipEventEmitter: PropTypes.object.isRequired,
}

export default TooltipsMaster