import './Paper.less'
import React, { Component } from 'react'
import bem from 'Helpers/Bem/Bem.helper'
import cn from 'classnames'

export default class Paper extends Component {
    constructor (props) {
        super(props)
        this.blockName = 'Paper'
        this.bem = bem.with(this.blockName)
    }

    render () {
        const {
            children,
            className,
        } = this.props

        return (
            <div className={cn(this.bem(), className)}>
                {children}
            </div>
        )
    }
}

Paper.defaultProps = {
    className: "",
}
