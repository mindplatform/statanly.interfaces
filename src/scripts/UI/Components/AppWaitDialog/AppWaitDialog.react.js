import './AppWaitDialog.less'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import _b from 'bem-cn'
import classNames from 'classnames'

import Transition from 'react-transition-group/Transition'

import { withStyles } from 'material-ui/styles'
import blue from 'material-ui/colors/blue'
import AutorenewIcon from 'material-ui-icons/Autorenew'

const AnimationDuration = .6e3

const styles = theme => ({
    iconBlue: {
        fill: blue[500],
    },
})

class AppWaitDialog extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'AppWaitDialog'
        this._b = _b(this.boxClassName)
    }

    render () {
        const {
            iconBlue,
        } = this.props.classes

        const {
            in: inProp,
            fixed,
            absolute,
        } = this.props

        return (
            <Transition in={inProp} timeout={AnimationDuration}>
                {(state) => (
                    <section
                        className={
                            this._b.state(
                                Object.assign(
                                    { [state]:state },
                                    {
                                        fixed,
                                        absolute,
                                    },
                                )
                            )
                        }
                    >
                        <AutorenewIcon className={classNames(iconBlue, this._b('Spinner').toString())}/>
                    </section>
                )}
            </Transition>
        )
    }
}

AppWaitDialog.defaultProps = {
    fixed: true,
    absolute: false,
}

export default withStyles(styles)(AppWaitDialog)
