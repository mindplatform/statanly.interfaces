import './Header.less'
import React, { Component } from 'react'
import _b from 'bem-cn'

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { Link } from 'react-router-dom'

import { 
    translate,
} from 'react-i18next'

import Time from 'UI/Extra/Time/Time.react'
import GlobalsSelector from 'Redux/Selectors/Globals/Globals.selector'

class Header extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'Header'
        this._b = _b(this.boxClassName)

        this.onChangeLanguageClick = ::this.onChangeLanguageClick
    }

    onChangeLanguageClick () {
        const {
            i18n,
        } = this.props
        i18n.changeLanguage(i18n.language == 'ru' ? 'en' : 'ru')
    }

    render () {
        const {
            t,
            i18n,
        } = this.props

        return (
            <header className={this._b}>
                <Link
                    className={this._b('Logo')}
                    to="/"
                >
                    <span
                        className={this._b('Logo')('Icon')}
                    ></span>
                    <span className={this._b('Logo')('Title')}>Statanly</span>
                </Link>

                <a 
                    className={this._b('Logout')}
                    href="/account/logoff">
                    Logout
                </a>
            </header>
        )
    }
}

export default connect(
    state => ({
        globals: GlobalsSelector({ state }),
    }),
    dispatch => ({}),
)(translate('translations')(Header))
