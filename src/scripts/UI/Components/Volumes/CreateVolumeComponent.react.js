import './CreateVolumeComponent.less'
import React, { Component } from 'react'
import _b from 'bem-cn'

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import FileReaderHelper from 'Helpers/FileReader/FileReader.helper'

import VolumesHTTPSelector from 'Redux/Selectors/HTTP/Volumes.HTTP.selector'
import VolumesSelector from 'Redux/Selectors/Volumes/Volumes.selector'
import * as VolumesActions from 'Redux/Actions/Volumes/Volumes.actions'

import { withStyles } from 'material-ui/styles'
import Button from 'material-ui/Button'
import AddIcon from 'material-ui-icons/Add'
import InProcessIcon from 'material-ui-icons/Refresh'

import CreateVolumeDialog from './CreateVolumeDialog.react'

const styles = theme => ({
    uploadInput: {
        display: 'none',
    },
    uploadButton: {},
})

class CreateVolumeComponent extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'CreateVolumeComponent'
        this._b = _b(this.boxClassName)

        this.state = {
            volumePresetDialogVisible: false,
            volume: props.volume,
        }

        this.handleVolumeCreate = ::this.handleVolumeCreate
        this.handleNameChange = ::this.handleNameChange
        this.handleVolumePresetClose = ::this.handleVolumePresetClose
        this.handleUpload = ::this.handleUpload
    }

    handleVolumeCreate () {
        this.setState({
            volumePresetDialogVisible: true,
        })
    }

    handleNameChange (volume) {
        this.setState({
            volume,
        })
    }

    handleVolumePresetClose (shouldGoNext) {
        if (!shouldGoNext) {
            this.setState({
                volumePresetDialogVisible: false,
            })
        } else {
            this.refInput.click()
        }
    }

    handleUpload () {
        const {
            volume,
        } = this.state

        if (this.refInput.files && this.refInput.files[0])
            new FileReaderHelper(this.refInput.files[0], file => {
                this.props.VolumesActions.create({ volume, file })
            })

        this.setState({
            volumePresetDialogVisible: false,
        })
    }

    renderAddButton () {
        const {
            uploadButton,
        } = this.props.classes

        const {
            volumesHTTPState,
        } = this.props.http

        if (volumesHTTPState.getIn(['REMOTE_VOLUME_CREATE', 'REQUEST']))
            return (
                <Button fab color="primary" aria-label="add" component="span" className={uploadButton} disabled>
                    <InProcessIcon className={this._b('InProcessIcon').toString()}/>
                </Button>
            )
        else
            return (
                <Button fab color="primary" aria-label="add" component="span" className={uploadButton} onClick={this.handleVolumeCreate}>
                    <AddIcon/>
                </Button>
            )
    }

    render () {
        const {
            uploadInput,
            uploadButton,
        } = this.props.classes

        const {
            volumePresetDialogVisible,
            volume,
        } = this.state

        return (
            <div className={this._b}>
                <CreateVolumeDialog
                    visible={volumePresetDialogVisible}
                    volume={volume}
                    onRequestClose={this.handleVolumePresetClose}
                    onNameChange={this.handleNameChange}
                />
                <form className={this._b('Form')}>
                    <input ref={ref => this.refInput = ref} hidden name="table" accept="" className={uploadInput} id="file" type="file" onChange={this.handleUpload}/>
                    {this.renderAddButton()}
                </form>
            </div>
        )
    }
}

export default connect(
    state => ({
        volume: VolumesSelector({ state }).blankVolume,
        http: VolumesHTTPSelector({ state }),
    }),
    dispatch => ({
        VolumesActions: bindActionCreators(VolumesActions, dispatch),
    }),
)(withStyles(styles)(CreateVolumeComponent))
