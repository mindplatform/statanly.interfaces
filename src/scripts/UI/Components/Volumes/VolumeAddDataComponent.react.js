import './VolumeAddDataComponent.less'
import React, { Component } from 'react'

import bem from 'Helpers/Bem/Bem.helper'
import cn from 'classnames'

import FileReaderHelper from 'Helpers/FileReader/FileReader.helper'

import { withStyles } from 'material-ui/styles'
import Button from 'material-ui/Button'

const styles = theme => ({
    uploadInput: {},
    addDataButton: {},
})

class VolumeAddDataComponent extends Component {
    constructor (props) {
        super(props)
        this.blockName = 'VolumeAddDataComponent'
        this.bem = bem.with(this.blockName)

        this.handleUpload = ::this.handleUpload
        this.handleAddData = ::this.handleAddData
    }

    handleUpload () {
        const {
            volume,
            onAddData,
        } = this.props

        if (this.refInput.files && this.refInput.files[0]) {
            new FileReaderHelper(this.refInput.files[0], file => {
                onAddData({ volume, file })
            })
        }
    }

    handleAddData () {
        this.refInput.click()
    }

    render () {
        const {
            className,
        } = this.props

        const {
            uploadInput,
            addDataButton,
        } = this.props.classes

        return (
            <form className={cn(this.bem(), className)}>
                <input ref={ref => this.refInput = ref} hidden name="table" accept="" className={uploadInput} type="file" onChange={this.handleUpload}/>
                <Button component="span" className={addDataButton} onClick={this.handleAddData}>
                    Add Data
                </Button>
            </form>
        )
    }
}

VolumeAddDataComponent.defaultProps = {
    className: "",
    onAddData: () => {},
}

export default withStyles(styles)(VolumeAddDataComponent)
