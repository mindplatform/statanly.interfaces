import './ViewVolumeComponent.less'
import React, { Component } from 'react'
import { HashRouter, BrowserRouter, Switch, Route, Redirect } from 'react-router-dom'

import _b from 'bem-cn'
import moment from 'moment'

import { Link } from 'react-router-dom'
import { withRouter } from 'react-router'
import { withStyles } from 'material-ui/styles'
import Button from 'material-ui/Button'

import Card from 'UI/Components/Cards/Card.react'
import Paper from 'UI/Components/Paper/Paper.react'

import VolumeColumnsComponent from 'UI/Components/Volumes/VolumeColumnsComponent.react'
import DeleteVolumeDialog from 'UI/Extra/DeleteVolumeDialog/DeleteVolumeDialog.react'
import EditVolumeDialog from 'UI/Components/Volumes/Edit/EditVolumeDialog.react'
import DataMiningVolumeContainer from 'UI/Containers/Volumes/DataMiningVolumeContainer.react'
import ModelsBuilderVolumeContainer from 'UI/Containers/Volumes/ModelsBuilderVolumeContainer.react'
import AnalysisVolumeContainer from 'UI/Containers/Volumes/AnalysisVolumeContainer.react'

import VolumeTabsSwitcher from 'UI/Components/Volumes/VolumeTabsSwitcherComponent.react'
import SpinnerSimple from 'UI/Components/Spinner/Spinner.simple.react'
import VolumeAddDataComponent from 'UI/Components/Volumes/VolumeAddDataComponent.react'

import VOLUME_STATUS_TYPES from 'Types/Volume/VolumeStatus.types'

import VOLUME_SWITCHER_TAB_TYPES from 'Consts/VOLUME_SWITCHER_TAB_TYPES.const'

const styles = theme => ({
    deleteButton: {},
    editButton: {},
    restoreButton: {},
    addDataButton: {},
})

class ViewVolumeComponent extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'ViewVolumeComponent'
        this._b = _b(this.boxClassName)

        this.state = {
            deleteDialogVisible: false,
            editVolumeDialogVisible: false,
        }

        this.handleDelete = ::this.handleDelete
        this.handleEdit = ::this.handleEdit
        this.handleRestore = ::this.handleRestore
        this.handleAddData = ::this.handleAddData

        this.handleUpdateVolume = ::this.handleUpdateVolume
    }

    handleDelete (shouldBeDeleted) {
        if (shouldBeDeleted)
            this.props.onDelete()

        this.setState({
            deleteDialogVisible: false,
        })
    }

    handleEdit (shouldBeEdited) {
        this.setState({
            editVolumeDialogVisible: shouldBeEdited,
        })
    }

    handleAddData (...args) {
        this.props.onAddData(...args)
    }

    handleUpdateVolume (volume) {
        this.handleEdit(false)
        this.props.onEdit(volume)
    }

    handleRestore () {
        this.props.onRestore()
    }

    renderName (volume) {
        return (
            <div className={this._b('Base')('Name')}>
                {volume.get('name')}
            </div>
        )
    }

    renderDescription (volume) {
        return (
            <div className={this._b('Base')('Description')}>
                {volume.get('description')}
            </div>
        )
    }

    renderCreatedTime (volume) {
        return (
            <div className={this._b('Base')('Time')}>
                {moment(volume.get('createdDate')).format('DD MMM YYYY')}
            </div>
        )
    }

    renderRestoreButton () {
        const {
            volume,
        } = this.props
        const {
            restoreButton,
        } = this.props.classes

        return (
            <div className={this._b('Controls')('Restore')}>
                <Button component="span" className={restoreButton} onClick={this.handleRestore}>
                    Restore
                </Button>
            </div>
        )
    }

    renderDeleteButton () {
        const {
            volume,
        } = this.props
        const {
            deleteButton,
        } = this.props.classes

        return (
            <div className={this._b('Controls')('Delete')}>
                <Button component="span" color="accent" className={deleteButton} onClick={() => {
                    this.setState({
                        deleteDialogVisible: true,
                    })
                }}>
                    Delete
                </Button>
                <DeleteVolumeDialog
                    visible={this.state.deleteDialogVisible}
                    onRequestClose={this.handleDelete}
                />
            </div>
        )
    }

    renderEditButton () {
        const {
            volume,
        } = this.props
        const {
            editButton,
        } = this.props.classes

        return (
            <div className={this._b('Controls')('Edit')}>
                <Button component="span" className={editButton} onClick={() => this.handleEdit(true)}>
                    Edit
                </Button>
            </div>
        )
    }

    renderAddDataButton () {
        const {
            volume,
        } = this.props
        const {
            addDataButton,
        } = this.props.classes

        return (
            <VolumeAddDataComponent
                className={this._b('Controls')('AddData')}
                volume={volume}
                onAddData={this.handleAddData}
            />
        )
    }

    rendeVolumeTab () {
        const {
            volume,
            match,
        } = this.props

        const models = volume.get('models')
        const isModels = models ? !!models.size : false

        const {
            id,
            tab,
        } = match.params

        switch (tab) {
            case VOLUME_SWITCHER_TAB_TYPES.get('builder'):
                return (
                    <Card className={this._b('Card')}>
                        <ModelsBuilderVolumeContainer volume={volume} className={this._b('ModelsBuilder')}/>
                    </Card>
                )

            case VOLUME_SWITCHER_TAB_TYPES.get('analysis'):
                return (
                    <Card className={this._b('Card')}>
                        <AnalysisVolumeContainer volume={volume} className={this._b('Analysis')}/>
                    </Card>
                )

            case VOLUME_SWITCHER_TAB_TYPES.get('dataMining'):
                return (
                    <Card className={this._b('Card')}>
                        <DataMiningVolumeContainer volume={volume} className={this._b('DataMining')}/>
                    </Card>
                )

            default:
                return (
                    <Redirect to={`/v/${id}/${VOLUME_SWITCHER_TAB_TYPES.get(isModels ? 'analysis' : 'builder')}`} />
                )
        }
    }

    render () {
        const {
            volume,
            className,
        } = this.props

        if (!volume)
            return null

        console.log('\n\n', 'View Volume:', '\n')
        console.log(volume.toJS())

        const isDeleted = volume.get('isDeleted')

        const volumeStatusType = volume.getIn(['status', 'type'])
        const isVolumeLoading = volumeStatusType == VOLUME_STATUS_TYPES.get('Loading')

        let paper

        if (isVolumeLoading) {
            paper = (
                <Paper className={this._b('Paper').toString()}>
                    <Card className={this._b('Card')}>
                        <div className={this._b('Base').state({
                            deleted: isDeleted,
                        })}>
                            {this.renderName(volume)}
                            {this.renderDescription(volume)}
                            {this.renderCreatedTime(volume)}
                        </div>
                        <div className={this._b('Spinner')}>
                            <SpinnerSimple/>
                        </div>
                    </Card>
                </Paper>
            )

        } else {
            paper = (
                <Paper className={this._b('Paper').toString()}>
                    <Card className={this._b('Card')}>
                        <div className={this._b('Base').state({
                            deleted: isDeleted,
                        })}>
                            {this.renderName(volume)}
                            {this.renderDescription(volume)}
                            {this.renderCreatedTime(volume)}
                            <VolumeColumnsComponent
                                volume={volume}
                                className={this._b('Base')('Columns').toString()}
                            />
                        </div>
                        <EditVolumeDialog
                            visible={this.state.editVolumeDialogVisible}
                            volume={this.props.volume}
                            onRequestClose={() => this.handleEdit(false)}
                            onRequestUpdate={this.handleUpdateVolume}
                        />
                        <div className={this._b('Controls')}>
                            {isDeleted ? this.renderRestoreButton() : null}
                            {isDeleted ? null : this.renderDeleteButton()}
                            {isDeleted ? null : this.renderEditButton()}
                            {isDeleted ? null : this.renderAddDataButton()}
                        </div>
                    </Card>
                    <Card className={this._b('Card').mix('Switcher')}>
                        <VolumeTabsSwitcher
                            className={this._b('Tabs')}
                            volume={volume}
                        />
                    </Card>
                    {this.rendeVolumeTab()}
                </Paper>
            )
        }

        return (
            <section className={this._b.mix(className)}>
                {paper}
            </section>
        )
    }
}

ViewVolumeComponent.defaultProps = {
    onDelete: () => {},
    onEdit: () => {},
    onRestore: () => {},
    onAddData: () => {},
}

export default withRouter(withStyles(styles)(ViewVolumeComponent))
