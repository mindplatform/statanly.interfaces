import './VolumeComponent.less'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import _b from 'bem-cn'

import moment from 'moment'

class VolumeComponent extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'VolumeComponent'
        this._b = _b(this.boxClassName)
    }

    renderName () {
        return (
            <div className={this._b('Name')}>
                {this.props.data.get('name')}
            </div>
        )
    }

    renderDescription () {
        return (
            <div className={this._b('Description')}>
                {this.props.data.get('description')}
            </div>
        )
    }

    renderCreatedTime () {
        return (
            <div className={this._b('Time')}>
                {moment(this.props.data.get('createdDate')).format('DD MMM YYYY')}
            </div>
        )
    }

    renderColumns () {
        return (
            <ol className={this._b('Columns')}>
                {this.props.data.get('columns').map((d,i) => (
                    <li key={`${i}-model`} className={this._b('Columns')('Item')}>
                        {`${d.get('name')}-${d.get('type')}`}
                    </li>
                ))}
            </ol>
        )
    }

    renderModels () {
        return (
            <ul className={this._b('Models')}>
                {this.props.data.get('models').map((d,i) => (
                    <li key={`${i}-model`} className={this._b('Models')('Item')}>
                        {d.get('name')}
                    </li>
                ))}
            </ul>
        )
    }

    render () {
        const isDeleted = this.props.data.get('isDeleted')

        return (
            <section className={this._b.state({
                deleted: isDeleted,
            })}>
                {this.renderName()}
                {this.renderDescription()}
                {this.renderCreatedTime()}
            </section>
        )
    }
}

export default VolumeComponent