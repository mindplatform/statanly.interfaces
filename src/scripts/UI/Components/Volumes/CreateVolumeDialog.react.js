import './CreateVolumeDialog.less'
import React, { Component } from 'react'
import _b from 'bem-cn'

import { withStyles } from 'material-ui/styles'
import TextField from 'material-ui/TextField'

import Button from 'material-ui/Button'
import Dialog, {
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
} from 'material-ui/Dialog'

const styles = theme => ({
    nameField: {
        width: '100%',
        margin: 0,
    },
})

class CreateVolumeDialog extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'CreateVolumeDialog'
        this._b = _b(this.boxClassName)

        this.handleRequestClose = ::this.handleRequestClose
        this.handleNameChange = ::this.handleNameChange
    }

    handleRequestClose (hasAgreed) {
        this.props.onRequestClose(hasAgreed)
    }

    handleNameChange (nextVolume) {
        this.props.onNameChange(nextVolume)
    }

    componentDidMount () {}

    componentWillUnmount () {}

    renderName () {
        const {
            nameField,
        } = this.props.classes

        return (
            <div className={this._b('Name')}>
                <TextField
                    required
                    id="name"
                    label="Name"
                    name="Name"
                    autoFocus={this.props.visible}
                    className={nameField}
                    value={this.props.volume.get('name') || ''}
                    onChange={event => {
                        const value = event.target.value
                        this.handleNameChange(this.props.volume.set('name', value))
                    }}
                    margin="normal"
                />
            </div>
        )
    }

    render () {
        return (
            <Dialog open={this.props.visible} onRequestClose={() => this.handleRequestClose(false)}>
                <DialogTitle>{"New data set:"}</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            {this.renderName()}
                        </DialogContentText>
                    </DialogContent>
                <DialogActions>
                    <Button onClick={() => this.handleRequestClose(false)} color="primary">
                        Cancel
                    </Button>
                    <Button disabled={!this.props.volume.get('name')} onClick={() => this.handleRequestClose(true)} color="accent">
                        Next
                    </Button>
                </DialogActions>
            </Dialog>
        )
    }
}

CreateVolumeDialog.defaultProps = {
    visible: false,
    onRequestClose: () => {},
    onNameChange: () => {},
}

export default withStyles(styles)(CreateVolumeDialog)
