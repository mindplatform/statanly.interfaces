import './VolumesComponent.less'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import _b from 'bem-cn'

import { Link } from 'react-router-dom'

import Card from 'UI/Components/Cards/Card.react'
import VolumeComponent from 'UI/Components/Volumes/VolumeComponent.react'

export default class VolumesComponent extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'VolumesComponent'
        this._b = _b(this.boxClassName)
    }

    renderTitle () {
        return (
            <div className={this._b('Title')}>
                Your data sets:
            </div>
        )
    }

    renderListItem (volume, index) {
        console.log(volume.toJS())

        return (
            <li
                key={`${index}-volume`}
                className={this._b('List')('Item')}
            >
                <Card className={this._b('List')('Item')('Card')}>
                    <Link to={`/v/${volume.get('id')}`} className={this._b('List')('Item')('Card')('Link')}>
                        <VolumeComponent data={volume}/>
                    </Link>
                </Card>
            </li>
        )
    }



    renderVolumes () {
        const {
            volumes,
        } = this.props

        if (!volumes || !volumes.size)
            return null

        console.log('\n\n', 'Volumes:', '\n')

        return (
            <ul className={this._b('List')}>
                {volumes.map((...args) => this.renderListItem(...args))}
            </ul>
        )
    }

    render () {
        return (
            <section className={this._b.mix(this.props.className)}>
                {this.renderTitle()}
                {this.renderVolumes()}
            </section>
        )
    }
}
