import './EditVolumeDialog.less'
import React, { Component } from 'react'
import _b from 'bem-cn'

import { withStyles } from 'material-ui/styles'
import Button from 'material-ui/Button'
import Dialog from 'material-ui/Dialog'
import Slide from 'material-ui/transitions/Slide'
import AppBar from 'material-ui/AppBar'
import Toolbar from 'material-ui/Toolbar'
import Typography from 'material-ui/Typography'

import IconButton from 'material-ui/IconButton'
import CloseIcon from 'material-ui-icons/Close'
import CheckIcon from 'material-ui-icons/Check'

import VolumeEdit from './VolumeEdit.react'

const styles = {
    appBar: {
        position: 'relative',
        backgroundColor: "rgba(19, 32, 40, 0.85)",
    },
    title: {
        flex: 1,
        fontWeight: 200,
        textAlign: "center",
    },
    updateButton: {},
    closeButton: {},
}

function Transition (props) {
    return <Slide direction="up" {...props} />
}

class EditVolumeDialog extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'EditVolumeDialog'
        this._b = _b(this.boxClassName)

        this.state = {
            volume: props.volume,
        }

        this.handleRequestClose = ::this.handleRequestClose
        this.handleRequestUpdate = ::this.handleRequestUpdate
        this.handleUpdateVolume = ::this.handleUpdateVolume
    }

    handleRequestClose () {
        this.props.onRequestClose()
    }

    handleRequestUpdate () {
        if (!this.refVolumeEditForm.reportValidity())
            return

        this.props.onRequestUpdate(this.state.volume)
    }

    handleUpdateVolume (volume) {
        this.setState({
            volume,
        })
    }

    componentDidMount () {}

    componentWillReceiveProps (nextProps) {
        this.setState({
            volume: nextProps.volume,
        })
    }

    componentWillUnmount () {}

    renderTitle () {
        return "Edit data set"
    }

    renderCloseButton () {
        const {
            closeButton,
        } = this.props.classes

        return (
            <IconButton onClick={this.handleRequestClose} className={closeButton} aria-label="Close" color="contrast">
                <CloseIcon />
            </IconButton>
        )
    }

    renderUpdateButton () {
        const {
            updateButton,
        } = this.props.classes

        const isDisabled = false

        return (
            <IconButton disabled={isDisabled} onClick={this.handleRequestUpdate} className={updateButton} aria-label="Done" color="contrast">
                <CheckIcon />
            </IconButton>
        )
    }

    renderToolBar () {
        const {
            title,
        } = this.props.classes

        return (
            <Toolbar>
                {this.renderCloseButton()}
                <Typography type="headline" color="inherit" className={title}>
                    {this.renderTitle()}
                </Typography>
                {this.renderUpdateButton()}
            </Toolbar>
        )
    }

    render () {
        const {
            appBar,
            title,
        } = this.props.classes

        return (
            <Dialog
                fullScreen
                open={this.props.visible}
                transition={Transition}
                onRequestClose={this.handleRequestClose}
            >
                <AppBar className={appBar}>
                    {this.renderToolBar()}
                </AppBar>

                <VolumeEdit
                    setFormRef={ref => this.refVolumeEditForm = ref}
                    volume={this.state.volume}
                    onChange={this.handleUpdateVolume}
                />

            </Dialog>
        )
    }
}

EditVolumeDialog.defaultProps = {
    visible: false,
    onRequestClose: () => {},
    onRequestUpdate: () => {},
}

export default withStyles(styles)(EditVolumeDialog)
