import './VolumeSkeletonEdit.less'
import React, { Component } from 'react'
import PropTypes from 'prop-types'

import _b from 'bem-cn'

import { withStyles } from 'material-ui/styles'
import Button from 'material-ui/Button'

import VolumeInfoEdit from './Components/VolumeInfoEdit.react'

const styles = theme => ({
    nextButton: {},
})

class VolumeSkeletonEdit extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'VolumeSkeletonEdit'
        this._b = _b(this.boxClassName)

        this.state = {
            volume: props.volume,
        }

        this.handleNext = ::this.handleNext
    }

    handleNext () {
        if (!this.refForm.reportValidity())
            return

        this.props.onNext(this.state.volume)
    }


    
    componentWillReceiveProps (nextProps) {
        this.setState({
            volume: nextProps.volume,
        })
    }



    renderTitle () {
        return (
            <div className={this._b('Form')('Title')}>
                Edit data set
            </div>
        )
    }

    renderNextButton () {
        const {
            nextButton,
        } = this.props.classes

        return (
            <div className={this._b('NavLine')('Next')}>
                <Button component="span" className={nextButton} onClick={this.handleNext}>
                    Next
                </Button>
            </div>
        )
    }

    render () {
        return (
            <section className={this._b.mix(this.props.className)}>
                <form ref={ref => this.refForm = ref} className={this._b('Form')}>
                    {this.renderTitle()}
                    <VolumeInfoEdit 
                        className={this._b('Form')('Line1')} 
                        volume={this.state.volume}
                        onChange={volume => this.setState({ volume })}
                    />
                </form>
                <div className={this._b('NavLine')}>
                    {this.renderNextButton()}
                </div>
            </section>
        )
    }
}

VolumeSkeletonEdit.propTypes = {
    volume: PropTypes.any.isRequired,
    classes: PropTypes.object,
    onNext: PropTypes.func,
}

VolumeSkeletonEdit.defaultProps = {
    className: '',
    onNext: () => {},
}

export default withStyles(styles)(VolumeSkeletonEdit)