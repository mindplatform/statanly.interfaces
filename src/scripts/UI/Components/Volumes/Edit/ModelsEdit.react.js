import './ModelsEdit.less'
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import _b from 'bem-cn'

import { withStyles } from 'material-ui/styles'
import Button from 'material-ui/Button'

import VolumeInfoEdit from './Components/VolumeInfoEdit.react'
import VolumeTableColumnsEdit from './Components/VolumeTableColumnsEdit.react'

const styles = theme => ({
    prevButton: {},
    doneButton: {},
})

class ModelsEdit extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'ModelsEdit'
        this._b = _b(this.boxClassName)

        this.state = {
            volume: props.volume,
        }
        
        this.handlePrev = ::this.handlePrev
        this.handleDone = ::this.handleDone
    }

    handlePrev () {
        if (!this.refForm.reportValidity())
            return

        this.props.onPrev(this.state.volume)
    }

    handleDone () {
        if (!this.refForm.reportValidity())
            return

        this.props.onDone(this.state.volume)
    }



    componentWillReceiveProps (nextProps) {
        this.setState({
            volume: nextProps.volume,
        })
    }



    renderTitle () {
        return (
            <div className={this._b('Form')('Line1')('Title')}>
                Edit data set
            </div>
        )
    }

    renderTableTitle () {
        return (
            <div className={this._b('Form')('Line2')('Title')}>
                Table
            </div>
        )   
    }

    renderPrevButton () {
        const {
            prevButton,
        } = this.props.classes

        return (
            <div className={this._b('NavLine')('Prev')}>
                <Button component="span" className={prevButton} onClick={this.handlePrev}>
                    Prev
                </Button>
            </div>
        )
    }

    renderDoneButton () {
        const {
            doneButton,
        } = this.props.classes

        return (
            <div className={this._b('NavLine')('Done')}>
                <Button component="span" color="primary" className={doneButton} onClick={this.handleDone}>
                    Done
                </Button>
            </div>
        )
    }

    render () {
        return (
            <section className={this._b.mix(this.props.className)}>
                <form ref={ref => this.refForm = ref} className={this._b('Form')}>
                    <section className={this._b('Form')('Line1')}>
                        {this.renderTitle()}
                        <VolumeInfoEdit 
                            className={this._b('Form')('Line1')('Body')} 
                            volume={this.state.volume}
                            disabled
                        />
                    </section>
                    <section className={this._b('Form')('Line2')}>
                        {this.renderTableTitle()}
                        <VolumeTableColumnsEdit 
                            className={this._b('Form')('Line2')('Body')}
                            volume={this.state.volume}
                            disabled
                        />
                    </section>
                </form>
                <div className={this._b('NavLine')}>
                    {this.renderPrevButton()}
                    {this.renderDoneButton()}
                </div>
            </section>
        )
    }
}

ModelsEdit.propTypes = {
    volume: PropTypes.any.isRequired,
    classes: PropTypes.object,
    onPrev: PropTypes.func,
    onDone: PropTypes.func,
}

ModelsEdit.defaultProps = {
    className: '',
    onPrev: () => {},
    onDone: () => {},
}

export default withStyles(styles)(ModelsEdit)