import './VolumeInfoEdit.less'
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import _b from 'bem-cn'

import { withStyles } from 'material-ui/styles'
import TextField from 'material-ui/TextField'

const styles = theme => ({
    nameField: {
        width: '100%',
        margin: 0,
    },
    descrField: {
        width: '100%',
        margin: 0,
    },
})

class VolumeInfoEdit extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'VolumeInfoEdit'
        this._b = _b(this.boxClassName)

        this.handleChange = ::this.handleChange
    }

    handleChange (nextVolume) {
        this.props.onChange(nextVolume)
    }

    renderName () {
        const {
            nameField,
        } = this.props.classes

        return (
            <div className={this._b('Name')}>
                <TextField
                    required
                    id="name"
                    label="Name"
                    name="Name"
                    className={nameField}
                    value={this.props.volume.get('name') || ''}
                    onChange={event => {
                        const value = event.target.value
                        this.handleChange(this.props.volume.set('name', value))
                    }}
                    margin="normal"
                    disabled={this.props.disabled}
                />
            </div>
        )
    }

    renderDescription () {
        const {
            descrField,
        } = this.props.classes

        return (
            <div className={this._b('Description')}>
                <TextField
                    id="name"
                    label="Description"
                    name="Description"
                    className={descrField}
                    value={this.props.volume.get('description') || ''}
                    onChange={event => {
                        const value = event.target.value
                        this.handleChange(this.props.volume.set('description', value))
                    }}
                    margin="normal"
                    disabled={this.props.disabled}
                />
            </div>
        )
    }

    render () {
        return (
            <div className={this._b.mix(this.props.className)}>
                {this.renderName()}
                {this.renderDescription()}
            </div>
        )
    }
}

VolumeInfoEdit.defaultProps = {
    className: '',
    disabled: false,
    onChange: () => {},
}

export default withStyles(styles)(VolumeInfoEdit)
