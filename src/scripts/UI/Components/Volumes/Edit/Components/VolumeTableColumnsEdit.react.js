import './VolumeTableColumnsEdit.less'
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import _b from 'bem-cn'

import TABLE_COLUMN_TYPES from 'Types/Table/TableColumn.types'

import { withStyles } from 'material-ui/styles'
import { FormControl } from 'material-ui/Form'
import Input, { InputLabel } from 'material-ui/Input'
import { MenuItem } from 'material-ui/Menu'
import Select from 'material-ui/Select'
import TextField from 'material-ui/TextField'

const styles = theme => ({
    formControl: {
        width: '100%',
        margin: 0,
    },
    columnNameField: {
        width: '100%',
        margin: 0,
    },
    columnDescriptionField: {
        width: '100%',
        margin: 0,
    },
})

class VolumeTableColumnsEdit extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'VolumeTableColumnsEdit'
        this._b = _b(this.boxClassName)

        this.handleChange = ::this.handleChange
    }

    handleChange (nextVolume) {
        this.props.onChange(nextVolume)
    }

    renderColumnIndex (d, index) {
        index += 1

        return (
            <div className={this._b('Columns')('Column')('Index')}>
                {`${index}.`}
            </div>
        )
    }

    renderType (value, key) {
        return (
            <MenuItem key={`${key}-${value}`} value={value}>{key}</MenuItem>
        )
    }

    renderColumnType (d, index) {
        const {
            formControl,
        } = this.props.classes

        return (
            <div className={this._b('Columns')('Column')('Type')}>
                <FormControl className={formControl} disabled={this.props.disabled}>
                    <InputLabel>Type *</InputLabel>
                    <Select
                        value={d.get('type')}
                        onChange={event => {
                            const value = event.target.value
                            this.handleChange(this.props.volume.setIn(['columns', index, 'type'], value))
                        }}
                    >
                        {TABLE_COLUMN_TYPES.map((...args) => this.renderType(...args))}
                    </Select>
                </FormControl>
            </div>
        )
    }

    renderColumnName (d, index) {
        const {
            columnNameField,
        } = this.props.classes

        return (
            <div className={this._b('Columns')('Column')('Name')}>
                <TextField
                    required
                    id={`column-${index}-name`}
                    label="Name"
                    name="Name"
                    className={columnNameField}
                    value={d.get('name') || ''}
                    onChange={event => {
                        const value = event.target.value
                        this.handleChange(this.props.volume.setIn(['columns', index, 'name'], value))
                    }}
                    margin="normal"
                    disabled={this.props.disabled}
                />
            </div>
        )
    }

    renderColumnDescription (d, index) {
        const {
            columnDescriptionField,
        } = this.props.classes

        return (
            <div className={this._b('Columns')('Column')('Description')}>
                <TextField
                    id={`column-${index}-description`}
                    label="Description"
                    name="Description"
                    className={columnDescriptionField}
                    value={d.get('description') || ''}
                    onChange={event => {
                        const value = event.target.value
                        this.handleChange(this.props.volume.setIn(['columns', index, 'description'], value))
                    }}
                    margin="normal"
                    disabled={this.props.disabled}
                />
            </div>
        )
    }

    renderColumn (d, index) {
        return (
            <li key={`column-${index}-name`} className={this._b('Columns')('Column')}>
                {this.renderColumnIndex(d, index)}
                {this.renderColumnType(d, index)}
                {this.renderColumnName(d, index)}
                {this.renderColumnDescription(d, index)}
            </li>
        )
    }

    renderColumns () {
        return (
            <ol className={this._b('Columns')}>
                {this.props.volume.get('columns').map((...args) => this.renderColumn(...args))}
            </ol>
        )
    }

    render () {
        return (
            <div className={this._b.mix(this.props.className)}>
                {this.renderColumns()}
            </div>
        )
    }
}

VolumeTableColumnsEdit.defaultProps = {
    className: '',
    disabled: false,
    onChange: () => {},
}

export default withStyles(styles)(VolumeTableColumnsEdit)
