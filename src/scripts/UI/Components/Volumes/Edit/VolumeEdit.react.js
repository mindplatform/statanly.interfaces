import './VolumeEdit.less'
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import _b from 'bem-cn'

import FileReaderHelper from 'Helpers/FileReader/FileReader.helper'

import { withStyles } from 'material-ui/styles'
import TextField from 'material-ui/TextField'
import Button from 'material-ui/Button'

import VolumeInfoEdit from './Components/VolumeInfoEdit.react'
import VolumeTableColumnsEdit from './Components/VolumeTableColumnsEdit.react'

const styles = theme => ({
    uploadInput: {
        display: 'none',
    },
    uploadButton: {},
})

class VolumeEdit extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'VolumeEdit'
        this._b = _b(this.boxClassName)

        this.handleUpload = ::this.handleUpload
    }

    handleUpload () {
        const {
            volume,
        } = this.props

        new FileReaderHelper(this.refInput.files[0], file => {
            this.props.onUpload({ volume, file })
        })
    }


    renderTableTitle () {
        return (
            <div className={this._b('Form')('Line2')('Flexible')('Title')}>
                Table
            </div>
        )
    }

    render () {
        return (
            <section className={this._b.mix(this.props.className)}>
                <form ref={ref => this.props.setFormRef(ref)} className={this._b('Form')}>
                    <section className={this._b('Form')('Line1')}>
                        <VolumeInfoEdit
                            className={this._b('Form')('Line1')('Fixed')}
                            volume={this.props.volume}
                            onChange={volume => this.props.onChange(volume)}
                        />
                    </section>
                    <section className={this._b('Form')('Line2')}>
                        <div className={this._b('Form')('Line2')('Flexible')}>
                            {this.renderTableTitle()}
                        </div>

                        <VolumeTableColumnsEdit
                            className={this._b('Form')('Line3')}
                            volume={this.props.volume}
                            onChange={volume => this.props.onChange(volume)}
                        />
                    </section>
                </form>
            </section>
        )
    }
}

VolumeEdit.propTypes = {
    volume: PropTypes.any.isRequired,
    classes: PropTypes.object,
    onUpload: PropTypes.func,
    onChange: PropTypes.func,
}

VolumeEdit.defaultProps = {
    className: '',
    onUpload: () => {},
    onChange: () => {},
}

export default withStyles(styles)(VolumeEdit)
