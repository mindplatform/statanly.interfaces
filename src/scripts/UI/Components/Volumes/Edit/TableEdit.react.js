import './TableEdit.less'
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import _b from 'bem-cn'

import FileReaderHelper from 'Helpers/FileReader/FileReader.helper'

import { withStyles } from 'material-ui/styles'
import TextField from 'material-ui/TextField'
import Button from 'material-ui/Button'

import VolumeInfoEdit from './Components/VolumeInfoEdit.react'
import VolumeTableColumnsEdit from './Components/VolumeTableColumnsEdit.react'

const styles = theme => ({
    uploadInput: {
        display: 'none',
    },
    uploadButton: {},

    prevButton: {},
    nextButton: {},
})

class TableEdit extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'TableEdit'
        this._b = _b(this.boxClassName)

        this.state = {
            volume: props.volume,
        }
        
        this.handlePrev = ::this.handlePrev
        this.handleNext = ::this.handleNext

        this.handleUpload = ::this.handleUpload
    }

    handlePrev () {
        if (!this.refForm.reportValidity())
            return

        this.props.onPrev(this.state.volume)
    }

    handleNext () {
        if (!this.refForm.reportValidity())
            return

        this.props.onNext(this.state.volume)
    }

    handleUpload () {
        new FileReaderHelper(this.refInput.files[0], file => {
            this.props.onUpload({ volume, file })
        })
    }



    componentWillReceiveProps (nextProps) {
        this.setState({
            volume: nextProps.volume,
        })
    }



    renderTitle () {
        return (
            <div className={this._b('Form')('Line1')('Title')}>
                Edit data set
            </div>
        )
    }

    renderTableTitle () {
        return (
            <div className={this._b('Form')('Line2')('Flexible')('Title')}>
                Table
            </div>
        )   
    }

    renderUploadButton () {
        const {
            uploadInput,
            uploadButton,
        } = this.props.classes

        return (
            <div className={this._b('Form')('Line2')('Flexible')('Upload')}>
                <input ref={ref => this.refInput = ref} name="table" accept="csv" className={uploadInput} id="file" type="file" onChange={this.handleUpload}/>
                <label htmlFor="file">
                    <Button raised dense component="span" className={uploadButton}>
                        Load New
                    </Button>
                </label>
            </div>
        )
    }

    renderPrevButton () {
        const {
            prevButton,
        } = this.props.classes

        return (
            <div className={this._b('NavLine')('Prev')}>
                <Button component="span" className={prevButton} onClick={this.handlePrev}>
                    Prev
                </Button>
            </div>
        )
    }

    renderNextButton () {
        const {
            nextButton,
        } = this.props.classes

        const isDisabled = !this.state.volume.get('columns').size

        return (
            <div className={this._b('NavLine')('Next')}>
                <Button disabled={isDisabled} component="span" className={nextButton} onClick={this.handleNext}>
                    Next
                </Button>
            </div>
        )
    }

    render () {
        return (
            <section className={this._b.mix(this.props.className)}>
                <form ref={ref => this.refForm = ref} className={this._b('Form')}>
                    <section className={this._b('Form')('Line1')}>
                        {this.renderTitle()}
                        <VolumeInfoEdit 
                            className={this._b('Form')('Line1')('Fixed')} 
                            volume={this.state.volume}
                        />
                    </section>
                    <section className={this._b('Form')('Line2')}>
                        <div className={this._b('Form')('Line2')('Flexible')}>
                            {this.renderTableTitle()}
                            {this.renderUploadButton()}
                        </div>

                        <VolumeTableColumnsEdit 
                            className={this._b('Form')('Line3')}
                            volume={this.state.volume}
                            onChange={volume => this.setState({ volume })}
                        />
                    </section>
                </form>
                <div className={this._b('NavLine')}>
                    {this.renderPrevButton()}
                    {this.renderNextButton()}
                </div>
            </section>
        )
    }
}

TableEdit.propTypes = {
    volume: PropTypes.any.isRequired,
    classes: PropTypes.object,
    onPrev: PropTypes.func,
    onNext: PropTypes.func,
    onUpload: PropTypes.func,
}

TableEdit.defaultProps = {
    className: '',
    onPrev: () => {},
    onNext: () => {},
    onUpload: () => {},
}

export default withStyles(styles)(TableEdit)
