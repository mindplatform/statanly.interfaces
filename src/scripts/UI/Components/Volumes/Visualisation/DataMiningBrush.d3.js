import * as d3 from 'd3'

export default class DataMiningBrush {
    constructor (svgRoot, onBrushEnd) {
        this.brushed = ::this.brushed
        this.brushended = ::this.brushended
        this.onBrushEnd = onBrushEnd

        this.svg = d3.select(svgRoot)

        this.brush = d3.brushY()
            .on("start brush", this.brushed)
            .on("end", this.brushended)

        this.g = this.svg.append("g")
    }

    render ({
        originWidth,
        originHeight,
    }) {
        const margin = {top: 20, right: 20, bottom: 30, left: 50}
        const width = originWidth - margin.left - margin.right
        const height = originHeight - margin.top - margin.bottom

        this.svg
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)

        this.g.attr("transform",
                "translate(" + margin.left + "," + margin.top + ")")
            .attr("class", "brush")
            .call(this.brush)
    }

    reset () {
        this.g.call(this.brush.move, [0,0])
    }

    brushed () {
        // in process
        this.onBrushEnd(d3.event.selection)
    }

    brushended () {
        this.onBrushEnd(d3.event.selection)
    }
}
