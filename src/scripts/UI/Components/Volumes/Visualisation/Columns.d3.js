import * as d3 from 'd3'

export default class Columns {
    constructor () {}

    render ({
        data,
        originWidth,
        originHeight,
        rootNode,
    }) {
        var margin = {top: 20, right: 20, bottom: 30, left: 50},
            width = originWidth - margin.left - margin.right,
            height = originHeight - margin.top - margin.bottom;

        // parse the date / time

        // set the ranges
        var x = d3.scaleLinear().range([0, width]);
        var y = d3.scaleLinear().range([height, 0]);

        // append the svg obgect to the body of the page
        // appends a 'group' element to 'svg'
        // moves the 'group' element to the top left margin
        d3.select(rootNode).selectAll('g').remove()

        var svg = d3.select(rootNode)
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
          .append("g")
            .attr("transform",
                  "translate(" + margin.left + "," + margin.top + ")");

        // Get the data

      // format the data
      data.forEach(function(d) {
          d.x = +d.x;
          d.close = +d.close;
      });

      // scale the range of the data
      x.domain([0, d3.max(data, function(d) { return d.x; })]);
      y.domain([0, d3.max(data, function(d) { return d.y; })]);

      // add the dots
      svg.selectAll("circle")
         .data(data)
         .enter().append("circle")
           .attr("r", 5)
           .attr("cx", function(d) { return x(d.x); })
           .attr("cy", function(d) { return y(d.y); });

      // add the X Axis
      svg.append("g")
          .attr("transform", "translate(0," + height + ")")
          .call(d3.axisBottom(x));

      // add the Y Axis
      svg.append("g")
          .call(d3.axisLeft(y));

      this.svg = svg
      this.x = x
      this.y = y
    }

    repaint ({
      brushSelection,
      onSelect,
    }) {
      const selectedList = []
      if (brushSelection) {
        const extent = brushSelection.map(this.y.invert, this.y)
        this.svg.selectAll("circle").classed("selected", d => {
          const isSelected = extent[1] <= d.y && d.y <= extent[0]
          return isSelected
        })
      } else {
        this.svg.selectAll("circle").classed("selected", false)
      }
    }
}
