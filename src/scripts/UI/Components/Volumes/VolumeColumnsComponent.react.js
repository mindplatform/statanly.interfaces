import './VolumeColumnsComponent.less'
import PropTypes from 'prop-types'
import React, { Component } from 'react'

import bem from 'Helpers/Bem/Bem.helper'

import TABLE_COLUMN_TYPES from 'Types/Table/TableColumn.types'

class VolumeColumn extends Component {
    render () {
        const {
            column,
            columnIndex,
            className,
            onSelectColumn,
            selectedColumn,
        } = this.props

        this.bem = bem.with(className || this.blockName)

        const isSelectable = !!onSelectColumn

        return (
            <li
                key={`${columnIndex}-model`} className={this.bem(null, {
                    selectabel: isSelectable,
                    active: column.get('name') == selectedColumn,
                })}
                onClick={() => isSelectable && onSelectColumn(column.get('name'))}
            >
                <span className={this.bem('Index')}>{columnIndex + 1}</span>
                <span className={this.bem('Name')}>{column.get('name')}</span>
                <span className={this.bem('Type')}>{TABLE_COLUMN_TYPES.entrySeq().find(e => e[1] == column.get('type'))[0]}</span>
            </li>
        )
    }
}

export default class VolumeColumnsComponent extends Component {
    constructor (props) {
        super(props)
        this.blockName = 'VolumeColumnsComponent'
    }

    render () {
        const {
            volume,
            className,
        } = this.props

        this.bem = bem.with(className || this.blockName)

        return (
            <div className={this.bem()}>
                <ol className={this.bem('List')}>
                    {volume.get('columns').map((column, columnIndex) => (
                        <VolumeColumn
                            {...this.props}
                            key={`${columnIndex}-model`}
                            column={column}
                            columnIndex={columnIndex}
                            className={this.bem('List-Item')}
                        />
                    ))}
                </ol>
            </div>
        )
    }
}

VolumeColumnsComponent.propTypes = {
    volume: PropTypes.any.isRequired,
    onSelectColumn: PropTypes.func,
    selectedColumn: PropTypes.string,
    className: PropTypes.any,
}

VolumeColumnsComponent.defaultProps = {
    onSelectColumn: null,
    selectedColumn: null,
    className: "",
}
