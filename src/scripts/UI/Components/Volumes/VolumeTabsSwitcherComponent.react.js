import './VolumeTabsSwitcherComponent.less'
import PropTypes from 'prop-types'
import React, { Component } from 'react'

import bem from 'Helpers/Bem/Bem.helper'

import { withRouter } from 'react-router'
import { withStyles } from 'material-ui/styles'

import Tabs, { Tab } from 'material-ui/Tabs'

import VOLUME_SWITCHER_TAB_TYPES from 'Consts/VOLUME_SWITCHER_TAB_TYPES.const'

const styles = theme => ({})

class VolumeTabsSwitcherComponent extends Component {
    constructor (props) {
        super(props)
        this.blockName = 'VolumeTabsSwitcherComponent'
        this.bem = bem.with(this.blockName)

        this.state = {
            tabs: [
                VOLUME_SWITCHER_TAB_TYPES.get('builder'),
                VOLUME_SWITCHER_TAB_TYPES.get('analysis'),
                VOLUME_SWITCHER_TAB_TYPES.get('dataMining'),
                VOLUME_SWITCHER_TAB_TYPES.get('mlTools'),
            ],
        }

        this.handleChange = ::this.handleChange
    }

    handleChange (event, value) {
        const {
            id,
        } = this.props.match.params
        const tab = this.state.tabs[value]
        this.props.history.push(`/v/${id}/${tab}`)
    }

    render () {
        const {
            tabs,
        } = this.state
        const {
            tab,
        } = this.props.match.params

        if (!tab)
            return null

        const valueInList = tabs.indexOf(tab)
        const value = ~valueInList ? valueInList : 0

        return (
            <Tabs
                value={value}
                onChange={this.handleChange}
                indicatorColor="primary"
                textColor="primary"
                fullWidth
            >
                <Tab label="Model Builder"/>
                <Tab label="Analysis" />
                <Tab label="Data Mining" />
                <Tab label="ML Tools" disabled/>
            </Tabs>
        )
    }
}

VolumeTabsSwitcherComponent.propTypes = {
    volume: PropTypes.any,
}

export default withRouter(withStyles(styles)(VolumeTabsSwitcherComponent))
