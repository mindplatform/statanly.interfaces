import './Card.less'
import React, { Component } from 'react'
import _b from 'bem-cn'

export default class Card extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'Card'
        this._b = _b(this.boxClassName)
    }

    render () {
        const {
            children,
            className,
        } = this.props
        return (
            <div className={this._b.mix(className)}>
                {children}
            </div>
        )
    }
}
