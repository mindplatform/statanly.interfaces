import './Footer.less'
import React, { Component } from 'react'
import _b from 'bem-cn'
import { Link } from 'react-router-dom'

class Footer extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'Footer'
        this._b = _b(this.boxClassName)
    }

    render () {
        return (
            <section className={this._b}>
                © 2017 Statanly Technologies. The statanly design is a trademark of statanly.com
            </section>
        )
    }
}

export default Footer