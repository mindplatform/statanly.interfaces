import './ModelTune.less'
import React, { Component } from 'react'
import _b from 'bem-cn'

import { withStyles } from 'material-ui/styles'
import Button from 'material-ui/Button'
import ButtonBase from 'material-ui/ButtonBase'

import TextField from 'material-ui/TextField'

import MathModelLearningTypes from 'Types/MathModel/MathModel.LearningType.types'
import MathModelProblemTypes from 'Types/MathModel/MathModel.ProblemType.types'

import TABLE_COLUMN_TYPES from 'Types/Table/TableColumn.types'
import MATH_MODEL_ALGORITHM_TYPES from 'Types/MathModel/MathModel.AlgorithmType.types'

const styles = {
    mapWidthField: {},
    mapHeightField: {},
}

class ModelTune extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'ModelTune'
        this._b = _b(this.boxClassName)

        this.state = {
            dependValues: [],
        }

        this.handleUpdateModel = ::this.handleUpdateModel
        this.handleDependValueSet = ::this.handleDependValueSet
        this.handleAlgorithmTypeSet = ::this.handleAlgorithmTypeSet
    }

    handleUpdateModel (model) {
        this.props.onModelUpdate(model)
    }

    handleDependValueSet (index) {
        this.handleUpdateModel(this.props.model.setIn(['settings', 'dependValue'], [index]))
    }

    handleAlgorithmTypeSet (algorithmType) {
        this.handleUpdateModel(this.props.model.setIn(['settings', 'algorithmType'], algorithmType))
    }

    renderColumn (d, index) {
        return (
            <li
                key={`column-${index}-name`}
                className={this._b('Columns')('Column').state({
                    selected: !!~this.props.model.getIn(['settings', 'dependValue']).indexOf(index),
                })}
                onClick={() => this.handleDependValueSet(index)}
            >
                <span className={this._b('Columns')('Column')('Index')}>{index + 1}</span>
                <span className={this._b('Columns')('Column')('Name')}>{d.get('name')}</span>
                <span className={this._b('Columns')('Column')('Type')}>{TABLE_COLUMN_TYPES.entrySeq().find(e => e[1] == d.get('type'))[0]}</span>
            </li>
        )
    }

    renderColumnSelect () {
        const {
            volume,
        } = this.props

        return (
            <ol className={this._b('Columns')}>
                {volume.get('columns').map((...args) => this.renderColumn(...args))}
            </ol>
        )
    }

    renderAlgorithmTypeItem (type, selected) {
        return (
            <li
                key={type}
                className={this._b('AlgorithmType')('Item').state({
                    selected,
                })}
                onClick={() => this.handleAlgorithmTypeSet(type)}
            >
                <div className={this._b('AlgorithmType')('Item')('Title')}>
                    {type.toUpperCase()}
                </div>
            </li>
        )
    }

    renderAlgorithmTypeSelect () {
        const {
            model,
        } = this.props

        const currentAlgorithmType = model.getIn(['settings', 'algorithmType'])

        return (
            <ol className={this._b('AlgorithmType')}>
                {MATH_MODEL_ALGORITHM_TYPES.map(type => this.renderAlgorithmTypeItem(type, type == currentAlgorithmType))}
            </ol>
        )
    }

    renderByType () {
        const {
            model,
        } = this.props

        switch (model.getIn(['settings', 'problemType'])) {
            case MathModelProblemTypes.get('Regression'):
            case MathModelProblemTypes.get('Classification'):
                return this.renderColumnSelect()

            case MathModelProblemTypes.get('Clustering'):
                return this.renderAlgorithmTypeSelect()
        }

        return null
    }

    render () {
        const {
            nextButton,
        } = this.props.classes

        return (
            <div className={this._b}>
                {this.renderByType()}
            </div>
        )
    }
}

ModelTune.defaultProps = {
    onModelUpdate: () => {},
}

export default withStyles(styles)(ModelTune)
