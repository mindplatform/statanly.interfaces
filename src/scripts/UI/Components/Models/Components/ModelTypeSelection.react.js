import './ModelTypeSelection.less'
import React, { Component } from 'react'
import _b from 'bem-cn'

import { withStyles } from 'material-ui/styles'
import Button from 'material-ui/Button'
import ButtonBase from 'material-ui/ButtonBase'

import MathModelLearningTypes from 'Types/MathModel/MathModel.LearningType.types'
import MathModelProblemTypes from 'Types/MathModel/MathModel.ProblemType.types'

const styles = {
    nextButton: {},
}

class ModelTypeSelection extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'ModelTypeSelection'
        this._b = _b(this.boxClassName)

        this.handleSelectRegression = ::this.handleSelectRegression
        this.handleSelectClassification = ::this.handleSelectClassification
        this.handleSelectClustering = ::this.handleSelectClustering
    }

    handleNextView (model) {
        this.props.onModelUpdate(model)
        this.props.onNextView()
    }

    handleSelectRegression () {
        const {
            model,
        } = this.props
        this.handleNextView(model
            .setIn(['settings', 'learningType'], MathModelLearningTypes.get('Supervised'))
            .setIn(['settings', 'problemType'], MathModelProblemTypes.get('Regression'))
        )
    }
    handleSelectClassification () {
        const {
            model,
        } = this.props
        this.handleNextView(model
            .setIn(['settings', 'learningType'], MathModelLearningTypes.get('Supervised'))
            .setIn(['settings', 'problemType'], MathModelProblemTypes.get('Classification'))
        )
    }
    handleSelectClustering () {
        const {
            model,
        } = this.props
        this.handleNextView(model
            .setIn(['settings', 'learningType'], MathModelLearningTypes.get('Unsupervised'))
            .setIn(['settings', 'problemType'], MathModelProblemTypes.get('Clustering'))
        )
    }


    render () {
        const {
            nextButton,
        } = this.props.classes

        return (
            <div className={this._b}>
                <div className={this._b('Regression')}>
                    <div
                        className={this._b('Regression')('Button').toString()}
                        onClick={this.handleSelectRegression}
                    >
                        <div className={this._b('Regression')('Icon')} />
                        <div className={this._b('Regression')('Title')}>
                            Regression
                        </div>
                    </div>
                </div>
                <div className={this._b('Classification')}>
                    <div
                        className={this._b('Classification')('Button').toString()}
                        onClick={this.handleSelectClassification}
                    >
                        <div className={this._b('Classification')('Icon')} />
                        <div className={this._b('Classification')('Title')}>
                            Classification
                        </div>
                    </div>
                </div>
                <div className={this._b('Clustering')}>
                    <div
                        className={this._b('Clustering')('Button').toString()}
                        onClick={this.handleSelectClustering}
                    >
                        <div className={this._b('Clustering')('Icon')} />
                        <div className={this._b('Clustering')('Title')}>
                            Clustering
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

ModelTypeSelection.defaultProps = {
    onNextView: () => {},
    onModelUpdate: () => {},
}

export default withStyles(styles)(ModelTypeSelection)
