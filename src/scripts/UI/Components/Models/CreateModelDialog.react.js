import './CreateModelDialog.less'
import React, { Component } from 'react'
import _b from 'bem-cn'

import { withStyles } from 'material-ui/styles'
import Button from 'material-ui/Button'
import Dialog from 'material-ui/Dialog'
import Slide from 'material-ui/transitions/Slide'
import AppBar from 'material-ui/AppBar'
import Toolbar from 'material-ui/Toolbar'
import Typography from 'material-ui/Typography'

import IconButton from 'material-ui/IconButton'
import CloseIcon from 'material-ui-icons/Close'
import ChevronLeftIcon from 'material-ui-icons/ChevronLeft'
import CheckIcon from 'material-ui-icons/Check'

import SwipeableViews from 'react-swipeable-views'

import ModelTypeSelection from './Components/ModelTypeSelection.react'
import ModelTune from './Components/ModelTune.react'

import MathModelProblemTypes from 'Types/MathModel/MathModel.ProblemType.types'

const styles = {
    appBar: {
        position: 'relative',
        backgroundColor: "rgba(19, 32, 40, 0.85)",
    },
    title: {
        flex: 1,
        fontWeight: 200,
        textAlign: "center",
    },
    prevButton: {},
    doneButton: {},
    closeButton: {},
}

function Transition (props) {
    return <Slide direction="up" {...props} />
}

class CreateModelDialog extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'CreateModelDialog'
        this._b = _b(this.boxClassName)

        this.state = {
            viewIndex: 0,
            model: props.model,
        }

        this.handleRequestClose = ::this.handleRequestClose
        this.handleRequestCreate = ::this.handleModelCreate
        this.handleNextView = ::this.handleNextView
        this.handlePrevView = ::this.handlePrevView
        this.handleUpdateModel = ::this.handleUpdateModel
    }

    handleRequestClose () {
        this.props.onRequestClose()
    }

    handleNextView () {
        this.setState(prevState => ({
            viewIndex: ++prevState.viewIndex,
        }))
    }

    handlePrevView () {
        this.setState(prevState => ({
            model: this.props.model,
            viewIndex: --prevState.viewIndex,
        }))
    }

    handleModelCreate () {
        this.props.onRequestCreate(this.state.model)
    }

    handleChangeIndex (nextIndex) {
        this.setState({ viewIndex: nextIndex })
    }

    handleUpdateModel (model) {
        this.setState({
            model,
        })
    }

    componentDidMount () {}

    componentWillReceiveProps (nextProps) {
        this.setState({
            model: nextProps.model,
        })

        if (nextProps.visible && !this.props.visible)
            this.setState({
                viewIndex: 0,
            })
    }

    componentWillUnmount () {}

    renderTitle () {
        const {
            viewIndex,
            model,
        } = this.state

        if (viewIndex == 0)
            return "Choose the problem"

        switch (model.getIn(['settings', 'problemType'])) {
            case MathModelProblemTypes.get('Regression'):
            case MathModelProblemTypes.get('Classification'):
                return "Select dependent column"

            case MathModelProblemTypes.get('Clustering'):
                return "Select algorithm type"
        }

        return ""
    }

    renderBackButton () {
        const {
            prevButton,
            closeButton,
        } = this.props.classes

        const {
            viewIndex,
        } = this.state

        if (viewIndex > 0)
            return (
                <IconButton onClick={this.handlePrevView} className={prevButton} aria-label="Back" color="contrast">
                    <ChevronLeftIcon />
                </IconButton>
            )

        return (
            <IconButton onClick={this.handleRequestClose} className={closeButton} aria-label="Close" color="contrast">
                <CloseIcon />
            </IconButton>
        )
    }

    renderDoneAndCloseButtonsMix () {
        const {
            doneButton,
        } = this.props.classes

        const {
            model,
            viewIndex,
        } = this.state

        if (viewIndex == this.SwipeableViews.length - 1) {
            let isDisabled
            switch (model.getIn(['settings', 'problemType'])) {
                case MathModelProblemTypes.get('Regression'):
                case MathModelProblemTypes.get('Classification'):
                    isDisabled = model.getIn(['settings', 'dependValue']).size == 0
                    break

                case MathModelProblemTypes.get('Clustering'):
                    isDisabled = !model.getIn(['settings', 'algorithmType'])
                    break
            }

            return (
                <IconButton disabled={isDisabled} onClick={this.handleRequestCreate} className={doneButton} aria-label="Done" color="contrast">
                    <CheckIcon />
                </IconButton>
            )
        }

        return null
    }

    renderToolBar () {
        const {
            title,
        } = this.props.classes

        return (
            <Toolbar>
                {this.renderBackButton()}
                <Typography type="headline" color="inherit" className={title}>
                    {this.renderTitle()}
                </Typography>
                {this.renderDoneAndCloseButtonsMix()}
            </Toolbar>
        )
    }

    render () {
        const {
            appBar,
            title,
        } = this.props.classes

        this.SwipeableViews = [
            <ModelTypeSelection
                key="ModelTypeSelection"
                model={this.state.model}
                onNextView={this.handleNextView}
                onModelUpdate={this.handleUpdateModel}
            />
            ,
            <ModelTune
                key="ModelTune"
                model={this.state.model}
                volume={this.props.volume}
                onModelUpdate={this.handleUpdateModel}
            />
        ]

        return (
            <Dialog
                fullScreen
                open={this.props.visible}
                transition={Transition}
                onRequestClose={this.handleRequestClose}
            >
                <AppBar className={appBar}>
                    {this.renderToolBar()}
                </AppBar>

                <SwipeableViews
                    index={this.state.viewIndex}
                    className={this._b('SwipeableViews').toString()}
                    containerStyle={{
                        height: "100%",
                    }}
                >
                    {this.SwipeableViews}
                </SwipeableViews>

            </Dialog>
        )
    }
}

CreateModelDialog.defaultProps = {
    visible: false,
    onRequestClose: () => {},
    onRequestCreate: () => {},
}

export default withStyles(styles)(CreateModelDialog)
