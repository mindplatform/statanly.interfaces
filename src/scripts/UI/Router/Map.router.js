import React from 'react'

import createBrowserHistory from 'history/createBrowserHistory'
import { createStore, compose, combineReducers, applyMiddleware } from 'redux'
import { Route, Router, browserHistory, IndexRoute } from 'react-router'
import { syncHistoryWithStore, routerReducer, routerMiddleware } from 'react-router-redux'
import { BrowserRouter } from 'react-router-dom'
import { Provider } from 'react-redux'

import {
    I18nextProvider,
} from 'react-i18next'
import i18n from 'Configuration/Localizations'

import thunk from 'redux-thunk'
import API_HTTP_M from 'Redux/Middleware/API.HTTP.middleware'
import SESSION_HTTP_PING_M from 'Redux/Middleware/Session/HTTP/Ping.HTTP.middleware'
import SESSION_HTTP_VOLUMES_M from 'Redux/Middleware/Session/HTTP/Volumes.HTTP.middleware'
import SESSION_HTTP_MODELS_M from 'Redux/Middleware/Session/HTTP/Models.HTTP.middleware'
import SESSION_HTTP_NOTIFICATIONS_M from 'Redux/Middleware/Session/HTTP/Notifications.HTTP.middleware'
import SESSION_LOCAL_VOLUMES_M from 'Redux/Middleware/Session/LOCAL/Volumes.LOCAL.middleware'

import * as Reducers from 'Redux/Reducers'

import App from 'UI/App/App.react'

const _browserHistory = createBrowserHistory()

const middleware = [
    routerMiddleware(_browserHistory),
    thunk,
    API_HTTP_M,
    SESSION_HTTP_PING_M,
    SESSION_HTTP_VOLUMES_M,
    SESSION_HTTP_MODELS_M,
    SESSION_HTTP_NOTIFICATIONS_M,
    SESSION_LOCAL_VOLUMES_M,
]

const store = createStore(
    combineReducers({
        ...Reducers,
        routing: routerReducer
    }),
    applyMiddleware(...middleware)
)

const history = syncHistoryWithStore(_browserHistory, store)

export default (
    <I18nextProvider i18n={i18n}>
        <Provider store={store}>
            <BrowserRouter history={history}>
                <Route path="/" component={App}/>
            </BrowserRouter>
        </Provider>
    </I18nextProvider>
)
