import './Loader.less'
import React, { Component, PropTypes, } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import _b from 'bem-cn'

import LoaderSelector from 'Redux/Selectors/Loader/Loader.selector'
import * as LoaderActions from 'Redux/Actions/Loader/Loader.actions'

class Loader extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'Loader'
        this._b = _b(this.boxClassName)
    }

    render () {
        const {
            isNetworkRequestInProcess,
        } = this.props

        if (!isNetworkRequestInProcess)
            return null

        return (
            <div className={this._b}>
                <div className={this._b('Center')}></div>
            </div>
        )
    }
}

export default connect(
    state => LoaderSelector({ state }),
    dispatch => ({
        LoaderActions: bindActionCreators(LoaderActions, dispatch),
    })
)(Loader)
