import './Volumes.less'
import React, { Component } from 'react'
import _b from 'bem-cn'

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import VolumesSelector from 'Redux/Selectors/Volumes/Volumes.selector'

import Header from 'UI/Components/Header/Header.react'
import VolumesComponent from 'UI/Components/Volumes/VolumesComponent.react'
import CreateVolumeComponent from 'UI/Components/Volumes/CreateVolumeComponent.react'
import Footer from 'UI/Components/Footer/Footer.react'
import Loader from 'UI/Loader/Loader.react'

class Volumes extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'Volumes'
        this._b = _b(this.boxClassName)
    }

    componentDidMount () {
        window.scrollTo(0,0)
    }

    rendeEmpty () {
        return (
            <div className={this._b('Empty')}>
                Load your first data set!
            </div>
        )
    }

    renderVolumes () {
        const {
            volumes,
        } = this.props

        if (!volumes || !volumes.size)
            return this.rendeEmpty()
        else
            return <VolumesComponent volumes={volumes.reverse()} className={this._b('VolumesComponent')}/>
    }

    render () {
        return (
            <section className={this._b}>
                <Header/>
                {this.renderVolumes()}
                <CreateVolumeComponent/>
                <Footer/>
            </section>
        )
    }
}

export default connect(
    state => VolumesSelector({ state }),
)(Volumes)
