import './ViewModel.less'
import React, { Component } from 'react'
import _b from 'bem-cn'

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import VolumesSelector from 'Redux/Selectors/Volumes/Volumes.selector'

import Header from 'UI/Components/Header/Header.react'
import ViewModelAnalysisContainer from 'UI/Containers/Models/ViewModelAnalysisContainer.react'
import ViewModelTableAnalysisContainer from 'UI/Containers/Models/ViewModelTableAnalysisContainer.react'
import Footer from 'UI/Components/Footer/Footer.react'
import Loader from 'UI/Loader/Loader.react'

class ViewModel extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'ViewModel'
        this._b = _b(this.boxClassName)
    }

    componentDidMount () {
        window.scrollTo(0,0)
    }

    rendeEmpty () {
        return (
            <div className={this._b('Empty')}>
                There is no such model :(
            </div>
        )
    }

    renderModelAnalysis () {
        const {
            volume,
            model,
        } = this.props

        if (volume && model) {
            if (model.getIn(['settings', 'problemType']) == 3) {
                return (
                    <ViewModelAnalysisContainer
                        volume={volume}
                        model={model}
                        className={this._b('ViewModelAnalysisContainer')}
                    />
                )
            } else {
                return (
                    <ViewModelTableAnalysisContainer
                        volume={volume}
                        model={model}
                        className={this._b('ViewModelTableAnalysisContainer')}
                    />
                )
            }
        } else
            return this.rendeEmpty()
    }

    render () {
        return (
            <section className={this._b}>
                <Header/>
                {this.renderModelAnalysis()}
                <Footer/>
            </section>
        )
    }
}

export default connect(
    (state, ownProps) => {
        const volume = VolumesSelector({ state }).volumes.find(v => v.get('id') == ownProps.match.params.vid)
        const model = volume ? volume.get('models').find(m => m.get('id') == ownProps.match.params.mid) : null
        return {
            volume,
            model,
        }
    },
    dispatch => ({}),
)(ViewModel)
