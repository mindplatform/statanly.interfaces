import './ViewVolume.less'
import React, { Component } from 'react'
import _b from 'bem-cn'

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { withRouter } from 'react-router'

import VolumesSelector from 'Redux/Selectors/Volumes/Volumes.selector'
import * as VolumesActions from 'Redux/Actions/Volumes/Volumes.actions'

import Header from 'UI/Components/Header/Header.react'
import ViewVolumeComponent from 'UI/Components/Volumes/ViewVolumeComponent.react'
import Footer from 'UI/Components/Footer/Footer.react'
import Loader from 'UI/Loader/Loader.react'

import VOLUME_STATUS_TYPES from 'Types/Volume/VolumeStatus.types'

class ViewVolume extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'ViewVolume'
        this._b = _b(this.boxClassName)

        this.handleDelete = ::this.handleDelete
        this.handleRestore = ::this.handleRestore
        this.handleEdit = ::this.handleEdit
        this.handleVolumeStatus = ::this.handleVolumeStatus
        this.handleAddData = ::this.handleAddData

        this.handleVolumeStatus(props)
    }

    handleDelete () {
        const {
            volume,
        } = this.props

        this.props.VolumesActions.deleteVolume({ volume })
    }

    handleRestore () {
        const {
            volume,
        } = this.props

        this.props.VolumesActions.restoreVolume({ volume })
    }

    handleEdit (volume) {
        this.props.VolumesActions.updateVolume({ volume })
    }

    handleAddData (...args) {
        this.props.VolumesActions.loadTable(...args)
    }

    handleVolumeStatus (props) {
        const {
            volume,
            VolumesActions,
        } = props

        const GET_VOLUME_TIMEOUT = 5e3

        if (volume) {
            const volumeStatusType = volume.getIn(['status', 'type'])
            if (volumeStatusType == VOLUME_STATUS_TYPES.get('Loading')) {
                setTimeout(() => VolumesActions.getVolume({
                    volumeId: volume.get('id'),
                }), GET_VOLUME_TIMEOUT)
            }
        }
    }

    componentDidMount () {
        window.scrollTo(0,0)
    }

    componentWillReceiveProps (nextProps) {
        this.handleVolumeStatus(nextProps)
    }

    rendeEmpty () {
        return (
            <div className={this._b('Empty')}>
                There is no such data set :(
            </div>
        )
    }

    renderVolume () {
        const {
            volume,
        } = this.props

        if (volume)
            return <ViewVolumeComponent
                        volume={volume}
                        onDelete={this.handleDelete}
                        onRestore={this.handleRestore}
                        onEdit={this.handleEdit}
                        onAddData={this.handleAddData}
                        className={this._b('ViewVolumeComponent')}
                    />
        else
            return this.rendeEmpty()
    }

    render () {
        return (
            <section className={this._b}>
                <Header/>
                {this.renderVolume()}
                <Footer/>
            </section>
        )
    }
}

export default withRouter(connect(
    (state, ownProps) => ({
        volume: VolumesSelector({ state }).volumes.find(d => d.get('id') == ownProps.match.params.id)
    }),
    dispatch => ({
        VolumesActions: bindActionCreators(VolumesActions, dispatch),
    }),
)(ViewVolume))
