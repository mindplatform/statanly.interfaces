import './Home.less'
import React, { Component } from 'react'
import _b from 'bem-cn'

import Header from 'UI/Components/Header/Header.react'
import MapBox from 'UI/Components/MapBox/MapBox.react'
import ModalWindow from 'UI/ModalWindow/ModalWindow.react'
import Footer from 'UI/Components/Footer/Footer.react'
import Loader from 'UI/Loader/Loader.react'

export default class Home extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'Home'
        this._b = _b(this.boxClassName)
    }

    render () {
        return (
            <section className={this._b}>
                <Header/>
                <ModalWindow/>
                <Footer/>
                <Loader/>
            </section>
        )
    }
}