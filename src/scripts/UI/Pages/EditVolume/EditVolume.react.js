import './EditVolume.less'
import React, { Component } from 'react'
import _b from 'bem-cn'

import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import VolumesSelector from 'Redux/Selectors/Volumes/Volumes.selector'
import * as VolumesActions from 'Redux/Actions/Volumes/Volumes.actions'

import Header from 'UI/Components/Header/Header.react'
import VolumeSkeletonEditComponent from 'UI/Components/Volumes/Edit/VolumeSkeletonEdit.react'
import TableEditComponent from 'UI/Components/Volumes/Edit/TableEdit.react'
import ModelsEditComponent from 'UI/Components/Volumes/Edit/ModelsEdit.react'
import Footer from 'UI/Components/Footer/Footer.react'
import Loader from 'UI/Loader/Loader.react'

class EditVolume extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'EditVolume'
        this._b = _b(this.boxClassName)

        this.state = {
            step: 0,
        }

        this.handleChangeStep = ::this.handleChangeStep
        this.handleFormDataUpload = ::this.handleFormDataUpload
    }

    handleLinkTransfer () {
        this.props.history.goBack()
    }

    handleSaveData (volume) {
        this.props.VolumesActions.updateVolume({ volume })
    }

    handleChangeStep (volume, state) {
        this.handleSaveData(volume)
        this.setState(state)
    }

    handleFormDataUpload (volume, file) {
        this.props.VolumesActions.loadTable({ volume, file })
    }

    handleDoneEdit (volume) {
        this.handleSaveData(volume)
        this.handleLinkTransfer()
    }

    rendeEmpty () {
        return (
            <div className={this._b('Empty')}>
                No Volume Found
            </div>
        )
    }

    renderStep () {
        const {
            volume,
        } = this.props

        if (!volume)
            return this.rendeEmpty()

        const {
            step,
        } = this.state

        switch (step) {
            case 0:
                return <VolumeSkeletonEditComponent 
                    className={this._b('VolumeSkeletonEditComponent').toString()}
                    volume={volume} 
                    onNext={volume => this.handleChangeStep(volume, {step:1})}
                />
            case 1:
                return <TableEditComponent 
                    className={this._b('TableEditComponent').toString()}
                    volume={volume} 
                    onPrev={volume => this.handleChangeStep(volume, {step:0})}
                    onNext={volume => this.handleChangeStep(volume, {step:2})}
                    onUpload={({ volume, file }) => this.handleFormDataUpload(volume, file)}
                />
            case 2:
                return <ModelsEditComponent 
                    className={this._b('ModelsEditComponent').toString()}
                    volume={volume} 
                    onPrev={volume => this.handleChangeStep(volume, {step:1})}
                    onDone={volume => this.handleDoneEdit(volume)}
                />
        }

        return null
    }

    render () {
        return (
            <section className={this._b}>
                <Header/>
                {this.renderStep()}
                <Footer/>
                <Loader/>
            </section>
        )
    }
}

export default withRouter(connect(
    (state, ownProps) => ({
        volume: VolumesSelector({ state }).volumes.find(d => d.get('id') == ownProps.match.params.id),
    }),
    dispatch => ({
        VolumesActions: bindActionCreators(VolumesActions, dispatch),
    }),
)(EditVolume))