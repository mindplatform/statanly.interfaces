import './Time.less'
import React, { Component } from 'react'
import _b from 'bem-cn'

import {
    getUTCMoment,
} from 'Helpers/Time/Moment.helper'

class Time extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'Time'
        this._b = _b(this.boxClassName)

        this.state = {
            time: getUTCMoment().format('MMMM Do YYYY, h:mm:ss a'),
        }
    }

    componentDidMount () {
        this.si = setInterval(() => {
            this.setState({
                time: getUTCMoment().format('MMMM Do YYYY, h:mm:ss a'),
            })
        }, 1e3)
    }

    componentWillUnmount () {
        clearInterval(this.si)
    }

    render () {
        return (
            <div className={this._b}>
                {this.state.time}
            </div>
        )
    }
}

export default Time