import './MainThread.less'
import React, { Component } from 'react'
import _b from 'bem-cn'

import { withStyles } from 'material-ui/styles'

import Button from 'material-ui/Button'
import Snackbar from 'material-ui/Snackbar'

const styles = theme => ({})

class MainThread extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'MainThread'
        this._b = _b(this.boxClassName)

        this.state = {
            visible: !!props.notification,
        }

        this.handleRequestClose = ::this.handleRequestClose
    }

    handleRequestClose () {
        this.setState({
            visible: false,
        })
    }

    componentDidMount () {}

    componentWillReceiveProps (nextProps) {
        this.setState({
            visible: this.props.notification != nextProps.notification,
        })
    }

    componentWillUnmount () {}

    render () {
        if (!this.props.notification)
            return null
        
        return (
            <Snackbar
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
                classes={this.props.classes}
                open={this.state.visible}
                autoHideDuration={4000}
                onRequestClose={this.handleRequestClose}
                SnackbarContentProps={{
                    'aria-describedby': 'message-id',
                }}
                message={
                    <span id="message-id">
                        {this.props.notification.get('message')}
                    </span>
                }
                action={[]}
            />
        )
    }
}

MainThread.defaultProps = {
    visible: false,
    notification: null,
}

export default withStyles(styles)(MainThread)
