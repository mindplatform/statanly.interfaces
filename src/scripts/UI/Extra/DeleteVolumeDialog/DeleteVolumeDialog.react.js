import './DeleteVolumeDialog.less'
import React, { Component } from 'react'
import _b from 'bem-cn'

import Button from 'material-ui/Button'
import Dialog, {
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
} from 'material-ui/Dialog'

class DeleteVolumeDialog extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'DeleteVolumeDialog'
        this._b = _b(this.boxClassName)

        this.handleRequestClose = ::this.handleRequestClose
    }

    handleRequestClose (hasAgreed) {
        this.props.onRequestClose(hasAgreed)
    }

    componentDidMount () {}

    componentWillUnmount () {}

    render () {
        return (
            <Dialog open={this.props.visible} onRequestClose={() => this.handleRequestClose(false)}>
                <DialogTitle>{"Ready for deleting your data set?"}</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            All the current operations on your data set will be stoped. You could restore your data set at any time.
                        </DialogContentText>
                    </DialogContent>
                <DialogActions>
                    <Button onClick={() => this.handleRequestClose(false)} color="primary">
                        Disagree
                    </Button>
                    <Button onClick={() => this.handleRequestClose(true)} color="accent">
                        Agree
                    </Button>
                </DialogActions>
            </Dialog>
        )
    }
}

DeleteVolumeDialog.defaultProps = {
    visible: false,
    onRequestClose: () => {},
}

export default DeleteVolumeDialog
