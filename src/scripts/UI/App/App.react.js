import './App.less'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import {
    Route,
    Switch,
    Redirect,
 } from 'react-router-dom'
import _b from 'bem-cn'

import SessionSelector from 'Redux/Selectors/Session/Session.selector'
import * as VolumesActions from 'Redux/Actions/Volumes/Volumes.actions'

import Volumes from 'UI/Pages/Volumes/Volumes.react'
import EditVolume from 'UI/Pages/EditVolume/EditVolume.react'
import ViewVolume from 'UI/Pages/ViewVolume/ViewVolume.react'
import ViewModel from 'UI/Pages/ViewModel/ViewModel.react'

import AppWaitDialog from 'UI/Components/AppWaitDialog/AppWaitDialog.react'
import Notifications from 'UI/Containers/Notifications/Notifications.react'

class App extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'App'
        this._b = _b(this.boxClassName)
    }

    componentDidMount () {
        this.props.VolumesActions.getVolumes()
    }

    renderBody () {
        return (
            <Switch>
                <Route exact path="/" component={Volumes}/>
                <Route exact path="/v/:id/:tab?" component={ViewVolume}/>
                <Route exact path="/m/:vid/:mid" component={ViewModel}/>
                <Redirect to={{
                    pathname: "/",
                    state: { referrer: location.pathname },
                }}/>
            </Switch>
        )
    }

    render () {
        return (
            <section
                className={ this._b }>
                <AppWaitDialog in={!this.props.isInitialized}/>
                {this.renderBody()}
                <Notifications/>
            </section>
        )
    }
}

export default connect(
    state => SessionSelector({ state }),
    dispatch => ({
        VolumesActions: bindActionCreators(VolumesActions, dispatch),
    })
)(App)
