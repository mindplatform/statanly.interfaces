import I from 'immutable'

export default I.fromJS({
    EVENTS: {
        SHOW: "SHOW",
        HIDE: "HIDE",
    },
    TYPES: {
        SINGLE: "SINGLE",
        GROUP: "GROUP",
    },
})