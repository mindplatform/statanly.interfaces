import I from 'immutable'

export default I.Map({
    builder: 'builder',
    analysis: 'analysis',
    'dataMining': 'data-mining',
    'mlTools': 'ml-tools',
})
