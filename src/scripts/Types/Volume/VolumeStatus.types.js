import I from 'immutable'

export default I.Map({
    Success: Number(0),
    Loading: Number(1),
    Failure: Number(2),
})
