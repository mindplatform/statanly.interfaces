import I from 'immutable'

export default I.OrderedMap({
    Null:           Number(0),
    Regression:     Number(1),
    Classification: Number(2),
    Clustering:     Number(3),
})