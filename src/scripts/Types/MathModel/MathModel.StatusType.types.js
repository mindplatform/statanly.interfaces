import I from 'immutable'

export default I.OrderedMap({
    NotCalculated:  Number(0),
    InProgress:     Number(1),
    Сalculated:     Number(2),
    Fail:           Number(3),
})