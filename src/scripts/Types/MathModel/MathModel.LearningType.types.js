import I from 'immutable'

export default I.OrderedMap({
    Supervised:     Number(0),
    Unsupervised:   Number(1),
})