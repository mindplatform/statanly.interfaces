import {
    updateVolume,
    VOLUME_ADD_MODEL,
    VOLUME_UPDATE_MODEL,
} from 'Redux/Actions/Volumes/Volumes.actions'

export default store => next => action => {

    next(action)

    switch (action.type) {
        case VOLUME_ADD_MODEL:
            (({
                volume,
                model,
            }) => {

                store.dispatch(updateVolume({
                    volume: volume.set('models', volume.get('models').push(model)),
                }))

            })(action)
            break

        case VOLUME_UPDATE_MODEL:
            (({
                volume,
                model,
            }) => {

                store.dispatch(updateVolume({
                    volume: volume.update('models', models => models.map(
                        prevModel => prevModel.get('id') == model.get('id') ? model : prevModel)
                    ),
                }))

            })(action)
            break
    }
}