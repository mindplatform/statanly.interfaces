import {
    pingHTTP,
    REMOTE_SERVICE_PING_REQUEST,
    REMOTE_SERVICE_PING_SUCCESS,
    REMOTE_SERVICE_PING_FAILURE,
} from 'Redux/Actions/API/HTTP/Ping.HTTP.actions'

import {
    setVolumes,
    GET_VOLUMES,
} from 'Redux/Actions/Volumes/Volumes.actions'

import praser from 'Helpers/Parsers/JSONParser.helper'
import * as Models from 'Models'

import PingDataPreset from 'Data/PingDataPreset'
import ModelPreset from 'Data/ModelPreset'

export default store => next => action => {

    next(action)

    switch (action.type) {
        case GET_VOLUMES:
            (() => {
                store.dispatch(pingHTTP({
                    body: JSON.stringify({
                        items:[
                            {key:0,entityJson:null},
                            {key:1,entityJson:null},
                            {key:2,entityJson:null},
                            {key:3,entityJson:null},
                            {key:4,entityJson:null},
                            {key:5,entityJson:null},
                            {key:6,entityJson:null},
                            {key:7,entityJson:null},
                        ],
                    }),
                }))
            })()
            break

        case REMOTE_SERVICE_PING_SUCCESS:
            (({
                items,
            }) => {

                if (items) {
                    items.map(d => {
                        switch (d.key) {
                            case 7:
                                store.dispatch(setVolumes({
                                    list: praser(d.entityJson).map(volume => new Models.Volume(volume)),
                                }))
                                break
                        }
                    })
                }

            })(_.get(action, 'response.data', {}) || {})
            break
    }
}
