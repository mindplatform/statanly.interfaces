import {
    createVolumeHTTP,
    REMOTE_VOLUME_CREATE_REQUEST,
    REMOTE_VOLUME_CREATE_SUCCESS,
    REMOTE_VOLUME_CREATE_FAILURE,

    tableLoadHTTP,
    REMOTE_VOLUME_TABLE_LOAD_REQUEST,
    REMOTE_VOLUME_TABLE_LOAD_SUCCESS,
    REMOTE_VOLUME_TABLE_LOAD_FAILURE,

    updateVolumeHTTP,
    REMOTE_VOLUME_UPDATE_REQUEST,
    REMOTE_VOLUME_UPDATE_SUCCESS,
    REMOTE_VOLUME_UPDATE_FAILURE,

    deleteVolumeHTTP,
    REMOTE_VOLUME_DELETE_REQUEST,
    REMOTE_VOLUME_DELETE_SUCCESS,
    REMOTE_VOLUME_DELETE_FAILURE,

    getVolumeHTTP,
    REMOTE_VOLUME_GET_REQUEST,
    REMOTE_VOLUME_GET_SUCCESS,
    REMOTE_VOLUME_GET_FAILURE,

    getVolumeDictionaryHTTP,
    REMOTE_VOLUME_GET_DICTIONARY_REQUEST,
    REMOTE_VOLUME_GET_DICTIONARY_SUCCESS,
    REMOTE_VOLUME_GET_DICTIONARY_FAILURE,
} from 'Redux/Actions/API/HTTP/Volumes.HTTP.actions'

import {
    setVolume,
    VOLUME_CREATE,
    VOLUME_LOAD_TABLE,
    VOLUME_UPDATE,
    VOLUME_DELETE,
    VOLUME_RESTORE,
    GET_VOLUME,

    setVolumeDictionary,
    VOLUME_GET_DICTIONARY,
} from 'Redux/Actions/Volumes/Volumes.actions'

import praser from 'Helpers/Parsers/JSONParser.helper'
import * as Models from 'Models'

import VOLUME_STATUS_TYPES from 'Types/Volume/VolumeStatus.types'

export default store => next => action => {

    next(action)

    switch (action.type) {
        case VOLUME_CREATE:
            (({
                volume,
                file,
            }) => {

                store.dispatch(createVolumeHTTP({
                    body: file,
                    search: {
                        name: volume.get('name'),
                        description: volume.get('description'),
                        start: 0,
                    },
                }))

            })(action)
            break

        case VOLUME_LOAD_TABLE:
            (({
                volume,
                file,
            }) => {

                store.dispatch(setVolume({
                    volume: volume.setIn(['status', 'type'], VOLUME_STATUS_TYPES.get('Loading')),
                }))
                store.dispatch(tableLoadHTTP({
                    body: file,
                    search: {
                        id: volume.get('id'),
                        start: 0,
                    },
                    echo: {
                        volume,
                    },
                }))

            })(action)
            break

        case REMOTE_VOLUME_TABLE_LOAD_SUCCESS:
            (({
                response,
                echo,
            }) => {
                if (response.isSuccess)
                    store.dispatch(setVolume({
                        volume: echo.volume.setIn(['status', 'type'], VOLUME_STATUS_TYPES.get('Success'))
                    }))

            })(action)
            break

        case REMOTE_VOLUME_CREATE_SUCCESS:
            (volume => {

                store.dispatch(setVolume({
                    volume: new Models.Volume(volume),
                }))

            })(_.get(action, 'response.data', {}) || {})
            break


        case VOLUME_UPDATE:
            (({
                volume,
            }) => {

                store.dispatch(updateVolumeHTTP({
                    body: JSON.stringify(volume.toJS()),
                    echo: {
                        volume,
                    },
                }))

            })(action)
            break


        case REMOTE_VOLUME_UPDATE_SUCCESS:
            ((volume, isSuccess) => {

                if (isSuccess)
                    store.dispatch(setVolume({
                        volume: new Models.Volume(volume),
                    }))

            })(
                _.get(action, 'response.data', {}) || {},
                _.get(action, 'response.isSuccess', false)
            )
            break


        case VOLUME_DELETE:
            (({
                volume,
            }) => {
                store.dispatch(deleteVolumeHTTP({
                    search: {
                        id: volume.get('id'),
                    },
                    echo: {
                        volume,
                    },
                }))

            })(action)
            break

        case REMOTE_VOLUME_DELETE_SUCCESS:
            (({
                response,
                echo,
            }) => {
                if (response.isSuccess)
                    store.dispatch(setVolume({
                        volume: echo.volume.setIn(['isDeleted'], true),
                    }))

            })(action)
            break

        case VOLUME_RESTORE:
            (({
                volume,
            }) => {
                const nextVolume = volume.setIn(['isDeleted'], false)
                store.dispatch(updateVolumeHTTP({
                    body: JSON.stringify(nextVolume.toJS()),
                    echo: {
                        volume: nextVolume,
                    },
                }))

            })(action)
            break


        case GET_VOLUME:
            (({
                volumeId,
            }) => {
                store.dispatch(getVolumeHTTP({
                    search: {
                        id: volumeId,
                    },
                }))

            })(action)
            break

        case REMOTE_VOLUME_GET_SUCCESS:
            (({
                response,
                echo,
            }) => {
                if (response.isSuccess)
                    store.dispatch(setVolume({
                        volume: new Models.Volume(response.data),
                    }))

            })(action)
            break


        case VOLUME_GET_DICTIONARY:
            (({
                volume,
            }) => {
                store.dispatch(getVolumeDictionaryHTTP({
                    search: {
                        tableId: volume.get('id'),
                    },
                }))

            })(action)
            break

        case REMOTE_VOLUME_GET_DICTIONARY_SUCCESS:
            (({
                response,
                echo,
            }) => {
                if (response.isSuccess)
                    store.dispatch(setVolumeDictionary({
                        dictionary: response.data,
                    }))

            })(action)
            break
    }
}
