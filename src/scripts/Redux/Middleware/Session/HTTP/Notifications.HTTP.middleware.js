import {
    REMOTE_SERVICE_PING_REQUEST,
    REMOTE_SERVICE_PING_SUCCESS,
    REMOTE_SERVICE_PING_FAILURE,
} from 'Redux/Actions/API/HTTP/Ping.HTTP.actions'

import {
    REMOTE_VOLUME_CREATE_REQUEST,
    REMOTE_VOLUME_CREATE_SUCCESS,
    REMOTE_VOLUME_CREATE_FAILURE,

    REMOTE_VOLUME_TABLE_LOAD_REQUEST,
    REMOTE_VOLUME_TABLE_LOAD_SUCCESS,
    REMOTE_VOLUME_TABLE_LOAD_FAILURE,

    REMOTE_VOLUME_UPDATE_REQUEST,
    REMOTE_VOLUME_UPDATE_SUCCESS,
    REMOTE_VOLUME_UPDATE_FAILURE,

    REMOTE_VOLUME_DELETE_REQUEST,
    REMOTE_VOLUME_DELETE_SUCCESS,
    REMOTE_VOLUME_DELETE_FAILURE,
} from 'Redux/Actions/API/HTTP/Volumes.HTTP.actions'

import {
    pushNotification,
} from 'Redux/Actions/Notifications/Notifications.actions'

export default store => next => action => {

    next(action)

    switch (action.type) {
        case REMOTE_SERVICE_PING_FAILURE:
        case REMOTE_VOLUME_CREATE_FAILURE:
        case REMOTE_VOLUME_TABLE_LOAD_FAILURE:
        case REMOTE_VOLUME_UPDATE_FAILURE:
        case REMOTE_VOLUME_DELETE_FAILURE:
            (() => {
                const {
                    status,
                    statusText,
                } = action.error

                const {
                    message,
                    stack,
                } = action.error

                store.dispatch(pushNotification({
                    message: `${action.type}: ${status || ""} ${statusText || message}`,
                }))
            })()
            break
    }
}
