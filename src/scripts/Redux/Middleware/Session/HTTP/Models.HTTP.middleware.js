import {
    buildModelHTTP,
    REMOTE_MODEL_BUILD_REQUEST,
    REMOTE_MODEL_BUILD_SUCCESS,
    REMOTE_MODEL_BUILD_FAILURE,

    calculateModelHTTP,
    REMOTE_MODEL_CALCULATE_REQUEST,
    REMOTE_MODEL_CALCULATE_SUCCESS,
    REMOTE_MODEL_CALCULATE_FAILURE,

    getModelHTTP,
    REMOTE_MODEL_GET_REQUEST,
    REMOTE_MODEL_GET_SUCCESS,
    REMOTE_MODEL_GET_FAILURE,
} from 'Redux/Actions/API/HTTP/Models.HTTP.actions'

import {
    setVolume,
} from 'Redux/Actions/Volumes/Volumes.actions'

import {
    MODEL_BUILD,
    MODEL_CALCULATE,
    MODEL_GET,

    setCalculatedModel,
} from 'Redux/Actions/Models/Models.actions'

import * as Models from 'Models'

export default store => next => action => {

    next(action)

    switch (action.type) {
        case MODEL_BUILD:
            (({
                volume,
                model,
            }) => {

                store.dispatch(buildModelHTTP({
                    search: {
                        tableId: volume.getIn(['id']),
                        modelId: model.getIn(['id']),
                    },
                    echo: {
                        volume,
                    },
                }))

            })(action)
            break

        case REMOTE_MODEL_BUILD_SUCCESS:
            ((isSuccess, model, {
                volume,
            }) => {

                if (isSuccess) {
                    model = new Models.MathModel(model)
                    store.dispatch(setVolume({
                        volume: volume.update('models', models => models.map(
                            prevModel => prevModel.get('id') == model.get('id') ? model : prevModel)
                        ),
                    }))
                }

            })(
                _.get(action, 'response.isSuccess', false),
                _.get(action, 'response.data', {}) || {},
                _.get(action, 'echo', {}) || {},
            )
            break

        case MODEL_CALCULATE:
            (({
                volume,
                model,
                file,
            }) => {

                store.dispatch(calculateModelHTTP({
                    body: file,
                    search: {
                        tableId: volume.getIn(['id']),
                        modelId: model.getIn(['id']),
                    },
                    echo: {
                        volume,
                        model,
                    },
                }))

            })(action)
            break

        case REMOTE_MODEL_CALCULATE_SUCCESS:
            ((isSuccess, calculatedModel, {
                volume,
                model,
            }) => {

                if (isSuccess) {
                    store.dispatch(setCalculatedModel({
                        sourceModel: model,
                        calculatedModel,
                    }))
                }

            })(
                _.get(action, 'response.isSuccess', false),
                _.get(action, 'response.data', {}) || {},
                _.get(action, 'echo', {}) || {},
            )
            break

        case MODEL_GET:
            (({
                volume,
                model,
            }) => {

                store.dispatch(getModelHTTP({
                    search: {
                        tableId: volume.getIn(['id']),
                        modelId: model.getIn(['id']),
                    },
                    echo: {
                        volume,
                        model,
                    },
                }))

            })(action)
            break

        case REMOTE_MODEL_GET_SUCCESS:
            ((isSuccess, nextModel, {
                volume,
                prevModel,
            }) => {

                if (isSuccess) {
                    const model = new Models.MathModel(nextModel)
                    store.dispatch(setVolume({
                        volume: volume.update('models', models => models.map(
                            prevModel => prevModel.get('id') == model.get('id') ? model : prevModel)
                        ),
                    }))
                }

            })(
                _.get(action, 'response.isSuccess', false),
                _.get(action, 'response.data', {}) || {},
                _.get(action, 'echo', {}) || {},
            )
            break
    }
}