import I from 'immutable'

import {
    REMOTE_SHARE_MAP_DATA_REQUEST, 
    REMOTE_SHARE_MAP_DATA_SUCCESS, 
    REMOTE_SHARE_MAP_DATA_FAILURE,
} from 'Redux/Actions/MapShare/MapShare.actions'

const DEFAULT_STATE = I.fromJS({
    state: {},
})

export default function MapShareReducer (state = DEFAULT_STATE, action) {
    switch (action.type) {

        case REMOTE_SHARE_MAP_DATA_REQUEST:
            return (nextState => nextState
                .setIn(['state', 'isRequestProcess'], true)
                .setIn(['state', 'isRequestSuccess'], false)
                .setIn(['state', 'isRequestFailure'], false)
            )(state)

        case REMOTE_SHARE_MAP_DATA_SUCCESS:
            return (nextState => nextState
                .setIn(['state', 'isRequestProcess'], false)
                .setIn(['state', 'isRequestSuccess'], true)
                .setIn(['state', 'isRequestFailure'], false)
            )(state)

        case REMOTE_SHARE_MAP_DATA_FAILURE:
            return (nextState => nextState
                .setIn(['state', 'isRequestProcess'], false)
                .setIn(['state', 'isRequestSuccess'], false)
                .setIn(['state', 'isRequestFailure'], true)
            )(state)
    }
    return state
}