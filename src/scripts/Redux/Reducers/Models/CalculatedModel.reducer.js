import I from 'immutable'

import {
    mapReduceHeatMapData,
    normilizeHeatMapData,
} from 'Helpers/MapReduce/MapReduce.helper'

import MATH_MODEL_PROBLEM_TYPES from 'Types/MathModel/MathModel.ProblemType.types'

import {
    MODEL_SET_CALCULATED,
} from 'Redux/Actions/Models/Models.actions'

const DEFAULT_STATE = I.fromJS({
    state: {},
    sourceModel: null,
    calculatedModel: null,
})

export default function CalculatedModelReducer (state = DEFAULT_STATE, action) {
    switch (action.type) {

        case MODEL_SET_CALCULATED:
            return (nextState => {
                const sourceModel = action.sourceModel
                let calculatedModel = action.calculatedModel

                if (sourceModel && sourceModel.getIn(['settings', 'problemType']) == MATH_MODEL_PROBLEM_TYPES.get('Clustering')) {
                    calculatedModel = calculatedModel.map(mapReduceHeatMapData)
                    calculatedModel = calculatedModel.map(normilizeHeatMapData)
                }

                return nextState
                    .setIn(['sourceModel'], sourceModel)
                    .setIn(['calculatedModel'], I.fromJS(calculatedModel))
            })(state)
            break
    }
    return state
}
