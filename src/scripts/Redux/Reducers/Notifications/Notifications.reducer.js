import I from 'immutable'

import {
    NOTIFICATION_PUSH,
} from 'Redux/Actions/Notifications/Notifications.actions'

const DEFAULT_STATE = I.fromJS({
    state: {},
    lastNotifications: I.List(),
})

export default function NotificationsReducer (state = DEFAULT_STATE, action) {
    switch (action.type) {
        case NOTIFICATION_PUSH:
            return (nextState => nextState
                .updateIn(['lastNotifications'], lastNotifications => lastNotifications.push(
                    I.Map({
                        message: action.message,
                        id: +new Date,
                    })
                ))
            )(state)
            break
    }
    return state
}
