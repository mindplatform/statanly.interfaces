import I from 'immutable'

import {
    GLOBALS_SET,
} from 'Redux/Actions/Globals/Globals.actions'

const DEFAULT_STATE = I.fromJS({
    state: {},
    data: {},
})

export default function GlobalsReducer (state = DEFAULT_STATE, action) {
    switch (action.type) {

        case GLOBALS_SET:
            return (nextState => nextState
                .setIn(['data'], I.fromJS(action.globals))
            )(state)
    }
    return state
}