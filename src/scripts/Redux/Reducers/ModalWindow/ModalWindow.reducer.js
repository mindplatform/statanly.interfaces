import I from 'immutable'

import {
    MODAL_WINDOW_SHOW,
    MODAL_WINDOW_HIDE,
} from 'Redux/Actions/ModalWindow/ModalWindow.actions'

const DEFAULT_STATE = I.fromJS({
    state: {
        active: false,
    },
})

export default function ModalWindowReducer (state = DEFAULT_STATE, action) {
    switch (action.type) {
        case MODAL_WINDOW_SHOW:
            return (nextState => nextState
                .setIn(['state', 'active'], true)
                .setIn(['state', 'bodyType'], action.bodyType)
                .setIn(['state', 'props'], action.props)
            )(state)
            break

        case MODAL_WINDOW_HIDE:
            return (nextState => nextState
                .setIn(['state', 'active'], false)
                .deleteIn(['state', 'bodyType'])
                .deleteIn(['state', 'props'])
            )(state)
            break
    }
    return state
}
