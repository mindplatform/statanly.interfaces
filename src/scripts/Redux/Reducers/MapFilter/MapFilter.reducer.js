import I from 'immutable'

import {
    MAP_FILTER_CHANGE_DATA_CATEGORY_VALUE,
    MAP_FILTER_CHANGE_BASE_VALUE,
    MAP_FILTER_CHANGE_PRICE_CHANGE_VALUE,
} from 'Redux/Actions/MapFilter/MapFilter.actions'

const DEFAULT_STATE = I.fromJS({
    state: {
        dataCategoryValue: 0,
        baseValue: 0,
        priceChangeValue: 0,
    },
})

export default function MapFilterReducer (state = DEFAULT_STATE, action) {
    switch (action.type) {

        case MAP_FILTER_CHANGE_DATA_CATEGORY_VALUE:
            return (nextState => {
                return nextState
                    .setIn(['state', 'dataCategoryValue'], action.nextDataCategoryValue)
            })(state)

        case MAP_FILTER_CHANGE_BASE_VALUE:
            return (nextState => {
                return nextState
                    .setIn(['state', 'baseValue'], action.nextBaseValue)
            })(state)

        case MAP_FILTER_CHANGE_PRICE_CHANGE_VALUE:
            return (nextState => {
                return nextState
                    .setIn(['state', 'priceChangeValue'], action.nextPriceChangeValue)
            })(state)
    }
    return state
}