import I from 'immutable'

import {
    REMOTE_SERVICE_PING_REQUEST,
    REMOTE_SERVICE_PING_SUCCESS,
    REMOTE_SERVICE_PING_FAILURE,
} from 'Redux/Actions/Ping/Ping.actions'

const DEFAULT_STATE = I.fromJS({
    state: {},
})

export default function PingReducer (state = DEFAULT_STATE, action) {
    switch (action.type) {
        case REMOTE_SERVICE_PING_REQUEST:
            return (nextState => nextState
                .setIn(['state', 'isRequestProcess'], true)
                .setIn(['state', 'isRequestSuccess'], false)
                .setIn(['state', 'isRequestFailure'], false)
            )(state)

        case REMOTE_SERVICE_PING_SUCCESS:
            return (nextState => nextState
                .setIn(['state', 'isRequestProcess'], false)
                .setIn(['state', 'isRequestSuccess'], true)
                .setIn(['state', 'isRequestFailure'], false)
            )(state)

        case REMOTE_SERVICE_PING_FAILURE:
            return (nextState => nextState
                .setIn(['state', 'isRequestProcess'], false)
                .setIn(['state', 'isRequestSuccess'], false)
                .setIn(['state', 'isRequestFailure'], true)
            )(state)
    }
    return state
}
