import I from 'immutable'

import {
    REMOTE_GET_MAP_DATA_REQUEST, 
    REMOTE_GET_MAP_DATA_SUCCESS, 
    REMOTE_GET_MAP_DATA_FAILURE,
} from 'Redux/Actions/MapData/MapData.actions'

import fakeMapData from 'Data/FakeMapData'

const DATA = fakeMapData.dataList.reduce(
    (m,l,y) => m.concat(
        l.map(
            (i,x) => ({
                x,
                y,
                i,
            })
        )
    )
, [])

const DEFAULT_STATE = I.fromJS({
    state: {},
    rawData: null,
    data: null,
    dataTimeStamp: null,
})

export default function MapDataReducer (state = DEFAULT_STATE, action) {
    switch (action.type) {

        case REMOTE_GET_MAP_DATA_REQUEST:
            return (nextState => nextState
                .setIn(['state', 'isRequestProcess'], true)
                .setIn(['state', 'isRequestSuccess'], false)
                .setIn(['state', 'isRequestFailure'], false)
            )(state)

        case REMOTE_GET_MAP_DATA_SUCCESS:
            return (nextState => nextState
                .setIn(['state', 'isRequestProcess'], false)
                .setIn(['state', 'isRequestSuccess'], true)
                .setIn(['state', 'isRequestFailure'], false)
                .setIn(['rawData'], I.fromJS(action.response.data))
                .setIn(['data'], I.fromJS(action.response.data.map(
                    d => d.reduce(
                        (m,l,y) => m.concat(
                            l.map(
                                (i,x) => ({
                                    x,
                                    y,
                                    i,
                                })
                            )
                        )
                    , [])
                )))
                .setIn(['dataTimeStamp'], +new Date)
            )(state)

        case REMOTE_GET_MAP_DATA_FAILURE:
            return (nextState => nextState
                .setIn(['state', 'isRequestProcess'], false)
                .setIn(['state', 'isRequestSuccess'], false)
                .setIn(['state', 'isRequestFailure'], true)
                .setIn(['rawData'], I.fromJS(DATA))
                .setIn(['data'], I.fromJS(DATA))
                .setIn(['dataTimeStamp'], +new Date)
            )(state)
    }
    return state
}