import I from 'immutable'
import * as VolumesHTTPActions from 'Redux/Actions/API/HTTP/Volumes.HTTP.actions'

const DEFAULT_STATE = I.fromJS({
    state: {},
})

export default function VolumesHTTPReducer (state = DEFAULT_STATE, action) {

    const actionType = VolumesHTTPActions[action.type]

    if (actionType) {
        const lodashSign = '_'
        const splitedType = actionType.split(lodashSign)
        const actionStateType = splitedType.slice(-1)
        const actionBaseType = splitedType.slice(0,-1).join(lodashSign)
        return state
            .setIn(['state', actionBaseType], I.Map({
                [actionStateType]: true,
            }))
    }

    return state
}
