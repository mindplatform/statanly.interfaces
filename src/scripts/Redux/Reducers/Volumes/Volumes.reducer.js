import I from 'immutable'

import {
    SET_VOLUMES,
    SET_VOLUME,
    SET_VOLUME_DICTIONARY,
} from 'Redux/Actions/Volumes/Volumes.actions'

const DEFAULT_STATE = I.fromJS({
    state: {},
    volumes: I.List(),
    dictionary: null,
})

export default function VolumesReducer (state = DEFAULT_STATE, action) {
    switch (action.type) {

        case SET_VOLUMES:
            return (nextState => nextState
                .setIn(['volumes'], I.List(action.list || []))
            )(state)
            break

        case SET_VOLUME:
            return (nextState => nextState
                .updateIn(['volumes'], volumes => {
                    const nextVolume = action.volume
                    const volumeEntry = volumes.findEntry(d => d.get('id') == nextVolume.get('id'))

                    if (volumeEntry)
                        return volumes.setIn([volumeEntry[0]], nextVolume)
                    else
                        return volumes.push(nextVolume)
                })
            )(state)
            break

        case SET_VOLUME_DICTIONARY:
            return (nextState => nextState
                .setIn(['dictionary'], I.fromJS(action.dictionary))
            )(state)
            break
    }
    return state
}
