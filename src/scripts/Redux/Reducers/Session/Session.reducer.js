import I from 'immutable'

import {
    REMOTE_SERVICE_PING_REQUEST,
    REMOTE_SERVICE_PING_SUCCESS,
    REMOTE_SERVICE_PING_FAILURE,
} from 'Redux/Actions/API/HTTP/Ping.HTTP.actions'

const DEFAULT_STATE = I.fromJS({
    state: {},
})

export default function SessionReducer (state = DEFAULT_STATE, action) {
    switch (action.type) {

        case REMOTE_SERVICE_PING_FAILURE:
        case REMOTE_SERVICE_PING_SUCCESS:
            return (nextState => nextState
                .setIn(['state', 'isInitialized'], true)
            )(state)
            break

    }
    return state
}
