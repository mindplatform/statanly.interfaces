export const MODAL_WINDOW_SHOW = "MODAL_WINDOW_SHOW"
export function showModalWindow ({
    bodyType,
    props,
}) {
    return {
        type: MODAL_WINDOW_SHOW,
        bodyType,
        props,
    }
}

export const MODAL_WINDOW_HIDE = "MODAL_WINDOW_HIDE"
export function hideModalWindow () {
    return {
        type: MODAL_WINDOW_HIDE,
    }
}