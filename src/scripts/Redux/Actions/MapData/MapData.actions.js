 import {
    API_CALL,
} from 'Redux/Middleware/API.HTTP.middleware'
import ROUTES from 'Routes/Routes.HTTP'

export const REMOTE_GET_MAP_DATA_REQUEST = "REMOTE_GET_MAP_DATA_REQUEST"
export const REMOTE_GET_MAP_DATA_SUCCESS = "REMOTE_GET_MAP_DATA_SUCCESS"
export const REMOTE_GET_MAP_DATA_FAILURE = "REMOTE_GET_MAP_DATA_FAILURE"

export function getRemoteMapData ({
    body,
    data,
    search,
    echo,
} = {}) {
    return {
        [API_CALL]: {
            route: ROUTES
                        .getIn(['model', 'calculate'])
                        .setIn(['search'], search),
            types: [
                REMOTE_GET_MAP_DATA_REQUEST, 
                REMOTE_GET_MAP_DATA_SUCCESS, 
                REMOTE_GET_MAP_DATA_FAILURE,
            ],
            data: {},
            echo: {},
        }
    }
}