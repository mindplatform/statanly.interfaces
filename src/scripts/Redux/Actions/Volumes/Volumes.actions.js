export const VOLUME_CREATE = "VOLUME_CREATE"
export function create ({
    volume,
    file,
} = {}) {
    return {
        type: VOLUME_CREATE,
        volume,
        file,
    }
}

export const VOLUME_LOAD_TABLE = "VOLUME_LOAD_TABLE"
export function loadTable ({
    volume,
    file,
} = {}) {
    return {
        type: VOLUME_LOAD_TABLE,
        volume,
        file,
    }
}

export const VOLUME_UPDATE = "VOLUME_UPDATE"
export function updateVolume ({
    volume,
} = {}) {
    return {
        type: VOLUME_UPDATE,
        volume,
    }
}

export const VOLUME_ADD_MODEL = "VOLUME_ADD_MODEL"
export function addVolumeModel ({
    volume,
    model,
} = {}) {
    return {
        type: VOLUME_ADD_MODEL,
        volume,
        model,
    }
}

export const VOLUME_UPDATE_MODEL = "VOLUME_UPDATE_MODEL"
export function updateVolumeModel ({
    volume,
    model,
} = {}) {
    return {
        type: VOLUME_UPDATE_MODEL,
        volume,
        model,
    }
}

export const SET_VOLUMES = "SET_VOLUMES"
export function setVolumes ({
    list,
} = {}) {
    return {
        type: SET_VOLUMES,
        list,
    }
}

export const SET_VOLUME = "SET_VOLUME"
export function setVolume ({
    volume,
} = {}) {
    return {
        type: SET_VOLUME,
        volume,
    }
}

export const GET_VOLUMES = "GET_VOLUMES"
export function getVolumes () {
    return {
        type: GET_VOLUMES,
    }
}

export const GET_VOLUME = "GET_VOLUME"
export function getVolume ({
    volumeId,
}) {
    return {
        type: GET_VOLUME,
        volumeId,
    }
}

export const VOLUME_DELETE = "VOLUME_DELETE"
export function deleteVolume ({
    volume,
}) {
    return {
        type: VOLUME_DELETE,
        volume,
    }
}

export const VOLUME_RESTORE = "VOLUME_RESTORE"
export function restoreVolume ({
    volume,
}) {
    return {
        type: VOLUME_RESTORE,
        volume,
    }
}

export const VOLUME_GET_DICTIONARY = "VOLUME_GET_DICTIONARY"
export function getVolumeDictionary ({
    volume,
}) {
    return {
        type: VOLUME_GET_DICTIONARY,
        volume,
    }
}

export const SET_VOLUME_DICTIONARY = "SET_VOLUME_DICTIONARY"
export function setVolumeDictionary ({
    dictionary,
}) {
    return {
        type: SET_VOLUME_DICTIONARY,
        dictionary,
    }
}
