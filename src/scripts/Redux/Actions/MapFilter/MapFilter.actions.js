export const MAP_FILTER_CHANGE_DATA_CATEGORY_VALUE = "MAP_FILTER_CHANGE_DATA_CATEGORY_VALUE"
export function changeDataCategoryValue (nextDataCategoryValue) {
    return {
        type: MAP_FILTER_CHANGE_DATA_CATEGORY_VALUE,
        nextDataCategoryValue,
    }
}

export const MAP_FILTER_CHANGE_BASE_VALUE = "MAP_FILTER_CHANGE_BASE_VALUE"
export function changeBaseValue (nextBaseValue) {
    return {
        type: MAP_FILTER_CHANGE_BASE_VALUE,
        nextBaseValue,
    }
}

export const MAP_FILTER_CHANGE_PRICE_CHANGE_VALUE = "MAP_FILTER_CHANGE_PRICE_CHANGE_VALUE"
export function changePriceChangeValue (nextPriceChangeValue) {
    return {
        type: MAP_FILTER_CHANGE_PRICE_CHANGE_VALUE,
        nextPriceChangeValue,
    }   
}