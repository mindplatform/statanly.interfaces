export const GLOBALS_SET = "GLOBALS_SET"
export function setGlobals (globals) {
    return {
        type: GLOBALS_SET,
        globals,
    }
}