export const NOTIFICATION_PUSH = "NOTIFICATION_PUSH"
export function pushNotification ({
    message,
}) {
    return {
        type: NOTIFICATION_PUSH,
        message,
    }
}
