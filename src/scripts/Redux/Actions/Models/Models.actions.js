export const MODEL_BUILD = "MODEL_BUILD"
export function buildModel ({
    volume,
    model,
} = {}) {
    return {
        type: MODEL_BUILD,
        volume,
        model,
    }
}

export const MODEL_CALCULATE = "MODEL_CALCULATE"
export function calculateModel ({
    volume,
    model,
    file,
} = {}) {
    return {
        type: MODEL_CALCULATE,
        volume,
        model,
        file,
    }
}

export const MODEL_GET = "MODEL_GET"
export function getModel ({
    volume,
    model,
} = {}) {
    return {
        type: MODEL_GET,
        volume,
        model,
    }
}

export const MODEL_SET_CALCULATED = "MODEL_SET_CALCULATED"
export function setCalculatedModel ({
    sourceModel,
    calculatedModel,
} = {}) {
    return {
        type: MODEL_SET_CALCULATED,
        sourceModel,
        calculatedModel,
    }
}
