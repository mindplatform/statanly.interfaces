import {
    API_CALL,
} from 'Redux/Middleware/API.HTTP.middleware'
import ROUTES from 'Routes/Routes.HTTP'

export const REMOTE_SHARE_MAP_DATA_REQUEST = "REMOTE_SHARE_MAP_DATA_REQUEST"
export const REMOTE_SHARE_MAP_DATA_SUCCESS = "REMOTE_SHARE_MAP_DATA_SUCCESS"
export const REMOTE_SHARE_MAP_DATA_FAILURE = "REMOTE_SHARE_MAP_DATA_FAILURE"

export function shareMapData ({
    data,
}) {
    return {
        [API_CALL]: {
            route: ROUTES.getIn(['shareMapData']),
            types: [
                REMOTE_SHARE_MAP_DATA_REQUEST, 
                REMOTE_SHARE_MAP_DATA_SUCCESS, 
                REMOTE_SHARE_MAP_DATA_FAILURE,
            ],
            data,
            echo: {},
        }
    }
}