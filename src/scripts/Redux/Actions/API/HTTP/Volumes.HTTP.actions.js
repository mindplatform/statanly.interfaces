import {
    API_CALL,
} from 'Redux/Middleware/API.HTTP.middleware'
import ROUTES from 'Routes/Routes.HTTP'

export const REMOTE_VOLUME_CREATE_REQUEST = "REMOTE_VOLUME_CREATE_REQUEST"
export const REMOTE_VOLUME_CREATE_SUCCESS = "REMOTE_VOLUME_CREATE_SUCCESS"
export const REMOTE_VOLUME_CREATE_FAILURE = "REMOTE_VOLUME_CREATE_FAILURE"

export function createVolumeHTTP ({
    data,
    body,
    search,
    echo,
} = {}) {
    return {
        [API_CALL]: {
            route: ROUTES
                        .getIn(['volumes', 'createVolume'])
                        .setIn(['search'], search),
            types: [
                REMOTE_VOLUME_CREATE_REQUEST,
                REMOTE_VOLUME_CREATE_SUCCESS,
                REMOTE_VOLUME_CREATE_FAILURE,
            ],
            data,
            body,
            echo,
        }
    }
}


export const REMOTE_VOLUME_TABLE_LOAD_REQUEST = "REMOTE_VOLUME_TABLE_LOAD_REQUEST"
export const REMOTE_VOLUME_TABLE_LOAD_SUCCESS = "REMOTE_VOLUME_TABLE_LOAD_SUCCESS"
export const REMOTE_VOLUME_TABLE_LOAD_FAILURE = "REMOTE_VOLUME_TABLE_LOAD_FAILURE"

export function tableLoadHTTP ({
    data,
    body,
    search,
    echo,
} = {}) {
    return {
        [API_CALL]: {
            route: ROUTES
                        .getIn(['volumes', 'tableLoad'])
                        .setIn(['search'], search),
            types: [
                REMOTE_VOLUME_TABLE_LOAD_REQUEST,
                REMOTE_VOLUME_TABLE_LOAD_SUCCESS,
                REMOTE_VOLUME_TABLE_LOAD_FAILURE,
            ],
            data,
            body,
            echo,
        }
    }
}


export const REMOTE_VOLUME_UPDATE_REQUEST = "REMOTE_VOLUME_UPDATE_REQUEST"
export const REMOTE_VOLUME_UPDATE_SUCCESS = "REMOTE_VOLUME_UPDATE_SUCCESS"
export const REMOTE_VOLUME_UPDATE_FAILURE = "REMOTE_VOLUME_UPDATE_FAILURE"

export function updateVolumeHTTP ({
    data,
    body,
    search,
    echo,
} = {}) {
    return {
        [API_CALL]: {
            route: ROUTES
                        .getIn(['volumes', 'updateVolume'])
                        .setIn(['search'], search),
            types: [
                REMOTE_VOLUME_UPDATE_REQUEST,
                REMOTE_VOLUME_UPDATE_SUCCESS,
                REMOTE_VOLUME_UPDATE_FAILURE,
            ],
            data,
            body,
            echo,
        }
    }
}


export const REMOTE_VOLUME_DELETE_REQUEST = "REMOTE_VOLUME_DELETE_REQUEST"
export const REMOTE_VOLUME_DELETE_SUCCESS = "REMOTE_VOLUME_DELETE_SUCCESS"
export const REMOTE_VOLUME_DELETE_FAILURE = "REMOTE_VOLUME_DELETE_FAILURE"

export function deleteVolumeHTTP ({
    data,
    body,
    search,
    echo,
} = {}) {
    return {
        [API_CALL]: {
            route: ROUTES
                        .getIn(['volumes', 'deleteVolume'])
                        .setIn(['search'], search),
            types: [
                REMOTE_VOLUME_DELETE_REQUEST,
                REMOTE_VOLUME_DELETE_SUCCESS,
                REMOTE_VOLUME_DELETE_FAILURE,
            ],
            data,
            body,
            echo,
        }
    }
}


export const REMOTE_VOLUME_GET_REQUEST = "REMOTE_VOLUME_GET_REQUEST"
export const REMOTE_VOLUME_GET_SUCCESS = "REMOTE_VOLUME_GET_SUCCESS"
export const REMOTE_VOLUME_GET_FAILURE = "REMOTE_VOLUME_GET_FAILURE"

export function getVolumeHTTP ({
    data,
    body,
    search,
    echo,
} = {}) {
    return {
        [API_CALL]: {
            route: ROUTES
                        .getIn(['volumes', 'getVolume'])
                        .setIn(['search'], search),
            types: [
                REMOTE_VOLUME_GET_REQUEST,
                REMOTE_VOLUME_GET_SUCCESS,
                REMOTE_VOLUME_GET_FAILURE,
            ],
            data,
            body,
            echo,
        }
    }
}


export const REMOTE_VOLUME_GET_DICTIONARY_REQUEST = "REMOTE_VOLUME_GET_DICTIONARY_REQUEST"
export const REMOTE_VOLUME_GET_DICTIONARY_SUCCESS = "REMOTE_VOLUME_GET_DICTIONARY_SUCCESS"
export const REMOTE_VOLUME_GET_DICTIONARY_FAILURE = "REMOTE_VOLUME_GET_DICTIONARY_FAILURE"

export function getVolumeDictionaryHTTP ({
    data,
    body,
    search,
    echo,
} = {}) {
    return {
        [API_CALL]: {
            route: ROUTES
                        .getIn(['volumes', 'getVolumeDictionary'])
                        .setIn(['search'], search),
            types: [
                REMOTE_VOLUME_GET_DICTIONARY_REQUEST,
                REMOTE_VOLUME_GET_DICTIONARY_SUCCESS,
                REMOTE_VOLUME_GET_DICTIONARY_FAILURE,
            ],
            data,
            body,
            echo,
        }
    }
}
