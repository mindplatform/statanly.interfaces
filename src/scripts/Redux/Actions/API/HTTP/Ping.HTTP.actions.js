import {
    API_CALL,
} from 'Redux/Middleware/API.HTTP.middleware'
import ROUTES from 'Routes/Routes.HTTP'

export const REMOTE_SERVICE_PING_REQUEST = "REMOTE_SERVICE_PING_REQUEST"
export const REMOTE_SERVICE_PING_SUCCESS = "REMOTE_SERVICE_PING_SUCCESS"
export const REMOTE_SERVICE_PING_FAILURE = "REMOTE_SERVICE_PING_FAILURE"

export function pingHTTP ({
    data,
    body,
    echo,
} = {}) {
    return {
        [API_CALL]: {
            route: ROUTES.getIn(['ping']),
            types: [
                REMOTE_SERVICE_PING_REQUEST,
                REMOTE_SERVICE_PING_SUCCESS,
                REMOTE_SERVICE_PING_FAILURE,
            ],
            data,
            body,
            echo,
        }
    }
}
