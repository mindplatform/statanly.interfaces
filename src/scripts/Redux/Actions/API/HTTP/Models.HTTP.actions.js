import {
    API_CALL,
} from 'Redux/Middleware/API.HTTP.middleware'
import ROUTES from 'Routes/Routes.HTTP'

export const REMOTE_MODEL_BUILD_REQUEST = "REMOTE_MODEL_BUILD_REQUEST"
export const REMOTE_MODEL_BUILD_SUCCESS = "REMOTE_MODEL_BUILD_SUCCESS"
export const REMOTE_MODEL_BUILD_FAILURE = "REMOTE_MODEL_BUILD_FAILURE"

export function buildModelHTTP ({
    body,
    data,
    search,
    echo,
} = {}) {
    return {
        [API_CALL]: {
            route: ROUTES
                        .getIn(['model', 'build'])
                        .setIn(['search'], search),
            types: [
                REMOTE_MODEL_BUILD_REQUEST,
                REMOTE_MODEL_BUILD_SUCCESS,
                REMOTE_MODEL_BUILD_FAILURE,
            ],
            data,
            body,
            echo,
        }
    }
}

export const REMOTE_MODEL_CALCULATE_REQUEST = "REMOTE_MODEL_CALCULATE_REQUEST"
export const REMOTE_MODEL_CALCULATE_SUCCESS = "REMOTE_MODEL_CALCULATE_SUCCESS"
export const REMOTE_MODEL_CALCULATE_FAILURE = "REMOTE_MODEL_CALCULATE_FAILURE"

export function calculateModelHTTP ({
    body,
    data,
    search,
    echo,
} = {}) {
    return {
        [API_CALL]: {
            route: ROUTES
                        .getIn(['model', 'calculate'])
                        .setIn(['search'], search),
            types: [
                REMOTE_MODEL_CALCULATE_REQUEST,
                REMOTE_MODEL_CALCULATE_SUCCESS,
                REMOTE_MODEL_CALCULATE_FAILURE,
            ],
            data,
            body,
            echo,
        }
    }
}

export const REMOTE_MODEL_GET_REQUEST = "REMOTE_MODEL_GET_REQUEST"
export const REMOTE_MODEL_GET_SUCCESS = "REMOTE_MODEL_GET_SUCCESS"
export const REMOTE_MODEL_GET_FAILURE = "REMOTE_MODEL_GET_FAILURE"

export function getModelHTTP ({
    body,
    data,
    search,
    echo,
} = {}) {
    return {
        [API_CALL]: {
            route: ROUTES
                        .getIn(['model', 'get'])
                        .setIn(['search'], search),
            types: [
                REMOTE_MODEL_GET_REQUEST,
                REMOTE_MODEL_GET_SUCCESS,
                REMOTE_MODEL_GET_FAILURE,
            ],
            data,
            body,
            echo,
        }
    }
}
