import I from 'immutable'
import { 
    createSelector,
} from 'reselect'

const lastNotifications = ({ state }) =>
    state.Notifications.getIn(['lastNotifications'])

export default createSelector(
    [ 
        lastNotifications,
    ],
    ( 
        lastNotifications,
    ) => ({
        lastNotifications,
    })
)
