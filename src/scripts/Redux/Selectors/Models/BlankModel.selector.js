import I from 'immutable'
import {
    createSelector,
} from 'reselect'

import * as Models from 'Models'

import MATH_MODEL_STATUS_TYPES from 'Types/MathModel/MathModel.StatusType.types'

export default createSelector(
    [],
    () => new Models.MathModel({
        id: null,
        name: "Test",
        createdDate: new Date,
        lastUpdate: new Date,
        settings: {
            maps: [
                {
                    width: 40,
                    height: 30,
                    size: 1200,
                },
                {
                    width: 70,
                    height: 50,
                    size: 3500,
                },
                {
                    width: 100,
                    height: 80,
                    size: 8000,
                },
            ],
        },
        status: {
            type: MATH_MODEL_STATUS_TYPES.get('InProgress'),
        }
    }),
)
