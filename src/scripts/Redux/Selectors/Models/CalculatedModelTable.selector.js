import I from 'immutable'
import {
    createSelector,
} from 'reselect'

const getSourceModel = ({ state }) =>
    state.CalculatedModel.getIn(['sourceModel'])

const getCalculatedModel = ({ state }) =>
    state.CalculatedModel.getIn(['calculatedModel'])

export default createSelector(
    [
        getSourceModel,
        getCalculatedModel,
    ],
    (
        sourceModel,
        calculatedModel,
    ) => ({
        sourceModel,
        calculatedModel,
    })
)
