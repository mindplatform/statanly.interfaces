import I from 'immutable'
import {
    createSelector,
} from 'reselect'

const getSourceModel = ({ state }) =>
    state.CalculatedModel.getIn(['sourceModel'])

const getCalculatedModel = ({ state }) =>
    state.CalculatedModel.getIn(['calculatedModel'])

const getCalculatedModelMinValue = ({ state }) =>
    (getCalculatedModel({ state }) || []).map(data => data.reduce((r, d) => d.value < r ? d.value : r, Infinity))

const getCalculatedModelMaxValue = ({ state }) =>
    (getCalculatedModel({ state }) || []).map(data => data.reduce((r, d) => d.value > r ? d.value : r, -Infinity))

export default createSelector(
    [
        getSourceModel,
        getCalculatedModel,
        getCalculatedModelMinValue,
        getCalculatedModelMaxValue,
    ],
    (
        sourceModel,
        calculatedModel,
        calculatedModelMinValue,
        calculatedModelMaxValue,
    ) => ({
        sourceModel,
        calculatedModel,
        calculatedModelMinValue,
        calculatedModelMaxValue,
    })
)
