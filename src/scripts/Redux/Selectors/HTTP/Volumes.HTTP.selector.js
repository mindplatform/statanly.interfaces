import I from 'immutable'
import {
    createSelector,
} from 'reselect'

const getVolumesHTTPState = ({ state }) =>
    state.VolumesHTTP.getIn(['state'])

export default createSelector(
    [
        getVolumesHTTPState,
    ],
    (
        volumesHTTPState,
    ) => ({
        volumesHTTPState,
    })
)
