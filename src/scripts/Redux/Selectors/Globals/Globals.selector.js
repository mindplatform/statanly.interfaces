import I from 'immutable'
import { 
    createSelector,
} from 'reselect'

const getTotalVolume24USD = ({ state }) =>
    state.Globals.getIn(['data', 'total_24h_volume_usd'])

const getTotalMarketCapUSD = ({ state }) =>
    state.Globals.getIn(['data', 'total_market_cap_usd'])

export default createSelector(
    [ 
        getTotalVolume24USD,
        getTotalMarketCapUSD,
    ],
    ( 
        totalVolume24USD,
        totalMarketCapUSD,
    ) => ({
        totalVolume24USD,
        totalMarketCapUSD,
    })
)
