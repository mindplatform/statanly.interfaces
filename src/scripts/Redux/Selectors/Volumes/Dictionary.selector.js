import {
    createSelector,
} from 'reselect'

const dictionary = ({ state }) =>
    state.Volumes.getIn(['dictionary'])

export default createSelector(
    [
        dictionary,
    ],
    (
        dictionary,
    ) => ({
        dictionary,
    })
)
