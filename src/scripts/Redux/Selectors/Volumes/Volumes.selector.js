import {
    createSelector,
} from 'reselect'

import * as Models from 'Models'
import {
    getUTCMoment,
} from 'Helpers/Time/Moment.helper'

const volumes = ({ state }) =>
    state.Volumes.getIn(['volumes'])

export default createSelector(
    [
        volumes,
    ],
    (
        volumes,
    ) => ({
        volumes,
        blankVolume: new Models.Volume({
            name: "",
            description: "",
        })
    })
)
