import I from 'immutable'
import {
    createSelector,
} from 'reselect'

const isNetworkRequestInProcess = ({ state }) =>
    state.MapData.getIn(['state', 'isRequestProcess']) ||
    state.MapShare.getIn(['state', 'isRequestProcess'])

export default createSelector(
    [
        isNetworkRequestInProcess,
    ],
    (
        isNetworkRequestInProcess,
    ) => ({
        isNetworkRequestInProcess,
    })
)
