import I from 'immutable'
import { 
    createSelector,
} from 'reselect'

import Types from 'Consts/Types.const'

const getMapData = ({ state }) =>
    state.MapData.getIn(['data'])

export default createSelector(
    [ 
        getMapData,
    ],
    ( 
        data,
    ) => ({
        data,
    })
)
