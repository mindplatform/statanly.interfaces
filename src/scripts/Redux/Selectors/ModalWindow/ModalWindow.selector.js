import { 
    createSelector,
} from 'reselect'

const isActive = ({ state }) =>
    state.ModalWindow.getIn(['state', 'active'])

const getBodyType = ({ state }) =>
    state.ModalWindow.getIn(['state', 'bodyType'])

const getProps = ({ state }) =>
    state.ModalWindow.getIn(['state', 'props'])

export default createSelector(
    [ 
        isActive,
        getBodyType,
        getProps,
    ],
    ( 
        active,
        bodyType,
        props,
    ) => ({
        active,
        bodyType,
        props,
    })
)
