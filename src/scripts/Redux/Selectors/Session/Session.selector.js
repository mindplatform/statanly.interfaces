import I from 'immutable'
import { 
    createSelector,
} from 'reselect'

const isInitialized = ({ state }) =>
    state.Session.getIn(['state', 'isInitialized'])

export default createSelector(
    [ 
        isInitialized,
    ],
    ( 
        isInitialized,
    ) => ({
        isInitialized,
    })
)
