import React from 'react'
import { render } from 'react-dom'
import RouterMap from 'UI/Router/Map.router'

import CONFIG from 'Configuration/Config'

import es6Promise from 'es6-promise'
import injectTapEventPlugin from 'react-tap-event-plugin'
import _b from 'bem-cn'

_b.setup({
    el: '__',
    mod: '--',
    modValue: '-',
})

if (CONFIG.get("CREADENTIALS")) {
    document.cookie = `${CONFIG.getIn(["CREADENTIALS", "cookie", "name"])}=${CONFIG.getIn(["CREADENTIALS", "cookie", "value"])},path=${CONFIG.getIn(["CREADENTIALS", "cookie", "path"])},domain=${CONFIG.getIn(["CREADENTIALS", "cookie", "domain"])}`
}

es6Promise.polyfill()
injectTapEventPlugin()

render(RouterMap, document.getElementById('Stage'))