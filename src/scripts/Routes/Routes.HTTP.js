import I from 'immutable'
export default I.fromJS({
    ping: {
        path: 'Exchange/Ping',
        params: {
            method: 'POST',
            headers: {},
            credentials: 'include',
        },
        search: {},
    },

    volumes: {
        createVolume: {
            path: 'Table/CreateFromFile',
            params: {
                method: 'POST',
                headers: {},
                credentials: 'include',
            },
            search: {},
        },
        tableLoad: {
            path: 'Data/Load',
            params: {
                method: 'POST',
                headers: {},
                credentials: 'include',
            },
            search: {},
        },
        updateVolume: {
            path: 'Table/Update',
            params: {
                method: 'POST',
                headers: {},
                credentials: 'include',
            },
            search: {},
        },
        deleteVolume: {
            path: 'Table/Delete',
            params: {
                method: 'GET',
                headers: {},
                credentials: 'include',
            },
            search: {},
        },
        getVolume: {
            path: 'Table/Get',
            params: {
                method: 'GET',
                headers: {},
                credentials: 'include',
            },
            search: {},
        },

        getVolumeDictionary: {
            path: 'Table/GetTableDictionary',
            params: {
                method: 'GET',
                headers: {},
                credentials: 'include',
            },
            search: {},
        },
    },


    model: {
        build: {
            path: 'Model/Build',
            params: {
                method: 'GET',
                headers: {},
                credentials: 'include',
            },
            search: {},
        },
        calculate: {
            path: 'Model/Calculate',
            params: {
                method: 'POST',
                headers: {},
                credentials: 'include',
            },
            search: {},
        },
        get: {
            path: 'Model/Get',
            params: {
                method: 'GET',
                headers: {},
                credentials: 'include',
            },
            search: {},
        },
    },


    getMapData: {
        path: 'http://statanly.com/Exchange/TestHeatMap',
        params: {
            method: 'GET',
            headers: {},
        },
        search: {
            width:40,
            height:30,
        },
    },
})