export default class WindowResizer {
  constructor () {
    this.resizeTimeout
  }

  addListener (handler) {
    this.handler = () => this.resizeThrottler(handler)
    window.addEventListener("resize", this.handler, false)  
  }

  removeListener () {
    window.removeEventListener("resize", this.handler)
  }

  resizeThrottler (handler) {
    if ( !this.resizeTimeout ) {
      this.resizeTimeout = setTimeout(() => {
        this.resizeTimeout = null
        handler()
        // The actualResizeHandler will execute at a rate of 15fps
       }, 66)
    }
  }
}