import * as d3 from 'd3'

export const mapReduceHeatMapData = d => d.reduce(
    (m,l,x) => m.concat(
        l.map(
            (value,y) => ({
                x,
                y,
                value,
            })
        )
    )
, [])

export const normilizeHeatMapData = data => {
    const dataMinValue = d3.min(data, d => d.value)
    const dataMaxValue = d3.max(data, d => d.value)

    const colorScale = d3.scaleLinear().domain([
        dataMinValue,
        dataMaxValue
    ]).range([0, 100])

    data.forEach(d => d.value = colorScale(d.value))

    return data
}