export default function JSONParser (string) {
    let parsed = null
    try {
        parsed = JSON.parse(string)
    } catch (e) {}

    return parsed
}