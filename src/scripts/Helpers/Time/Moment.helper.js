import _moment from 'moment'

export const moment = _moment

export function getUTCMoment (moment) {
    return _moment(moment || new Date)
}