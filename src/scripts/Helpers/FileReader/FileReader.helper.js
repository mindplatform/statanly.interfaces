export default class FileReaderHelper {
    constructor (file, onSuccess) {
        // const reader = new FileReader()
        // reader.onload = event => {
            const fileBlob = new Blob([file], {type: "text/plain"})
            onSuccess(fileBlob)
        // }
        // reader.readAsText(file)
    }
}