// import html2canvas from './html2canvas.min'

export default class Converter {
    constructor ({
        svgElement,
    }) {
        const rect = svgElement.getBoundingClientRect()        
        const svgString = new XMLSerializer().serializeToString(svgElement)
        const canvasElement = document.createElement('canvas')
        canvasElement.setAttribute('width', rect.width)
        canvasElement.setAttribute('height', rect.height)
        canvasElement.setAttribute('style', "display:none;")
        document.body.append(canvasElement)
        
        const ctx = canvasElement.getContext("2d")
        const DOMURL = self.URL || self.webkitURL || self
        const img = new Image()
        img.setAttribute('crossOrigin', '*')
        const svg = new Blob([svgString], {type: "image/svg+xml;charset=utf-8"})
        const url = DOMURL.createObjectURL(svg)

        const promise = new Promise((resolve, reject) => {
            img.onload = () => {
                ctx.drawImage(img, 0, 0)
                const base64 = canvasElement.toDataURL("image/jpeg")
                canvasElement.remove()
                console.log(base64)
                resolve(base64)
                // document.querySelector('#png-container').innerHTML = '<img src="'+png+'"/>'
                // DOMURL.revokeObjectURL(png)
            }    
        })
        img.src = url

        return promise

        // return new Promise((resolve, reject) => {
        //     const {
        //         width,
        //         height,
        //     } = svgElement.getBoundingClientRect()

        //     html2canvas(svgElement, {
        //         allowTaint: true,
        //         onrendered: function(canvas) {
        //             resolve(canvas.toDataURL("image/jpeg"))
        //         },
        //         width,
        //         height,
        //     })
        // })
    }
}