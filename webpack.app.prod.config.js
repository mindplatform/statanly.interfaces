"use strict"

const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const extractLESS = new ExtractTextPlugin('./content/spa/templates/home.css');

module.exports = {
    entry: [
        'whatwg-fetch',
        './src/app.entry.js'
    ],
    output: {
        path: path.resolve(__dirname, 'public'),
        publicPath: '/',
        filename: './scripts/app.bundle.js'
    },
    resolve: {
        modules: [
            path.resolve(__dirname, './src/scripts'),
            "node_modules",
        ]
    },
    module: {
        loaders: [
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    { loader: 'css-loader', options: { importLoaders: 1 } },
                    'postcss-loader',
                ]
            },
            {
                test: /\.less$/,
                exclude: /node_modules/,
                use: [
                    'style-loader',
                    { loader: 'css-loader', options: { importLoaders: 1 } },
                    'less-loader',
                    'postcss-loader',
                ],
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                include: path.resolve(__dirname, 'src'),
                query: {
                    presets: ['stage-0', 'es2015', 'react'],
                    plugins: ['transform-decorators-legacy']
                }
            },
            // {
            //     test: /\.less$/,
            //     exclude: /node_modules/,
            //     use: extractLESS.extract({
            //         fallback: 'style-loader',
            //         use: [
            //             { loader: 'css-loader', options: { importLoaders: 1 } },
            //             'less-loader',
            //             'postcss-loader',
            //         ]
            //     }),
            // },
            // {
            //     test: /\.less$/,
            //     use: ExtractTextPlugin.extract({
            //         fallback: "style-loader",
            //         use: "css-loader",
            //     }),
            // },
        ],
    },
    node: {
        fs: "empty",
    },
    plugins: [
        // extractLESS,
        new webpack.DefinePlugin({
            process: {
                env: {
                    NODE_ENV: JSON.stringify('production'),
                    APP_PORT: JSON.stringify(process.env.APP_PORT),
                    APP_LOG_LEVEL: JSON.stringify(process.env.APP_LOG_LEVEL)
                }
            }
        }),
        new webpack.optimize.UglifyJsPlugin({}),
    ],
}
